DROP SCHEMA IF EXISTS apoteka;
CREATE SCHEMA apoteka DEFAULT CHARACTER SET utf8;
USE apoteka;
CREATE TABLE proizvodjac_leka (
	id BIGINT AUTO_INCREMENT,
    naziv VARCHAR(25) NOT NULL,
    drzava_sedista CHAR(3) NOT NULL,
    PRIMARY KEY(id)
);
CREATE TABLE kategorija_leka (
	id BIGINT AUTO_INCREMENT,
    naziv VARCHAR(25) NOT NULL,
    namena VARCHAR(100) NOT NULL,
    opis VARCHAR(100) NOT NULL,
    PRIMARY KEY(id)
);
CREATE TABLE lek (
	id BIGINT AUTO_INCREMENT,
    naziv VARCHAR(25) NOT NULL,
    opis VARCHAR(100) NOT NULL,
    kontraindikacije VARCHAR(100) NOT NULL,
    oblik VARCHAR(25) NOT NULL,
    prosecna_ocena LONG  NOT NULL,
    slika VARCHAR(25) NOT NULL,
    dostupna_kolicina INT NOT NULL,
    cena BIGINT NOT NULL,
    proizvodjac_id BIGINT NOT NULL,
    kategorija_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (proizvodjac_id) REFERENCES proizvodjac_leka (id),
    FOREIGN KEY (kategorija_id) REFERENCES kategorija_leka(id)
);
CREATE TABLE korisnik (
	korisnicko_ime VARCHAR(25),
    lozinka VARCHAR(25) NOT NULL,
    email VARCHAR(25) UNIQUE NOT NULL,
    ime VARCHAR(25) NOT NULL,
    prezime VARCHAR(25) NOT NULL,
    datum_rodjenja DATE NOT NULL,
    adresa VARCHAR(25) NOT NULL,
    broj_telefona INT NOT NULL,
    datum_registracije DATETIME NOT NULL,
    uloga VARCHAR(25) NOT NULL,
    aktivan BIT NOT NULL,
    PRIMARY KEY(korisnicko_ime)
);
CREATE TABLE loyalty_kartica (
	email_korisnika VARCHAR(25),
    popust VARCHAR(25) NOT NULL,
    broj_poena INT NOT NULL,
    status VARCHAR(15) NOT NULL,
    PRIMARY KEY(email_korisnika)
);
CREATE TABLE komentar (
	id BIGINT AUTO_INCREMENT,
    tekst_komentara VARCHAR(100) NOT NULL,
    ocena INT NOT NULL,
    datum_objavljivanja DATE NOT NULL,
    autor_email VARCHAR(25),
    lek_id BIGINT,
    anoniman BIT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(autor_email) REFERENCES korisnik(email),
    FOREIGN KEY(lek_id) REFERENCES lek(id)
);
CREATE TABLE korpa (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    korisnik_email VARCHAR(25) NOT NULL,
    FOREIGN KEY (korisnik_email) REFERENCES korisnik(email)
);

CREATE TABLE korpa_item (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    korpa_id BIGINT NOT NULL,
    lek_id BIGINT NOT NULL,
    kolicina INT NOT NULL,
    FOREIGN KEY (korpa_id) REFERENCES korpa(id),
    FOREIGN KEY (lek_id) REFERENCES lek(id)
);

CREATE TABLE racun (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  korisnik_email VARCHAR(255) NOT NULL,
  timestamp DATE NOT NULL,
  cena DECIMAL(10, 2) NOT NULL,
  FOREIGN KEY (korisnik_email) REFERENCES korisnik(email)
);

CREATE TABLE racun_item (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  racun_id BIGINT NOT NULL,
  lek_id BIGINT NOT NULL,
  kolicina INT NOT NULL,
  FOREIGN KEY (racun_id) REFERENCES racun(id),
  FOREIGN KEY (lek_id) REFERENCES lek(id)
);

CREATE TABLE nabavka_lekova (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  apotekar_email VARCHAR(255),
  datum_i_vreme DATETIME,
  status ENUM ('PENDING', 'APPROVED', 'DENIED'),
  razlog_nabavke VARCHAR(255),
  razlog_odbijanja VARCHAR(255),
  lek_id BIGINT,
  kolicina_nabavke INT,
  FOREIGN KEY (apotekar_email) REFERENCES Korisnik(email),
  FOREIGN KEY (lek_id) REFERENCES Lek(id)
);


-- INSERT INTO korisnik VALUES ('user', 'user', 'user@mail.com', 'ime', 'prezime', '2001-11-24', 'ulica i broj', 0606060606, '2022-11-11 16:00:00', 'ADMINISTRATOR', true);

-- INSERT INTO proizvodjac_leka VALUES (1, 'Pahuljica, proizvodjac', 'SRB');
-- INSERT INTO kategorija_leka VALUES (1, 'da', 'da', 'da');
-- INSERT INTO lek VALUES (1, 'nazivLeka', 'opisLeka', 'kontraNesto', 'INJEKCIJA', 10, 'url slike', 99, 19999, 1, 1);

-- INSERT INTO komentar VALUES (1, 'tekst1', 1, '2023-1-1', 'user@mail.com', 1, false);
-- INSERT INTO komentar VALUES (2, 'tekst2', 4, '2023-1-1', 'user@mail.com', 1, false);
-- INSERT INTO komentar VALUES (3, 'tekst3', 5, '2023-1-1', 'user@mail.com', 1, false);
-- INSERT INTO komentar VALUES (4, 'tekst4', 3, '2023-1-1', 'user@mail.com', 1, false);


INSERT INTO proizvodjac_leka (naziv, drzava_sedista) VALUES
('Novartis', 'CHE'),
('Pfizer', 'USA'),
('Roche', 'CHE');

INSERT INTO kategorija_leka (naziv, namena, opis) VALUES
('Antibiotici', 'Protiv bakterijskih infekcija', 'Lekovi koji se koriste za suzbijanje bakterijskih infekcija'),
('Analgetici', 'Protiv bolova', 'Lekovi koji se koriste za ublažavanje bolova');

INSERT INTO lek (naziv, opis, kontraindikacije, oblik, prosecna_ocena, slika, dostupna_kolicina, cena, proizvodjac_id, kategorija_id) VALUES
('Paracetamol', 'Lek protiv bolova i povišene temperature', 'Nije preporučljiv za osobe sa jetrenim problemima', 'FILM_TABLETA', 4, 'paracetamol.jpg', 1000, 80, 1, 2),
('Amoksicilin', 'Antibakterijski lek', 'Nije preporučljiv za trudnice', 'KAPSULA', 3, 'amoksicilin.jpeg', 800, 120, 2, 1),
('Tamiflu', 'Lek protiv gripa', 'Nije preporučljiv za osobe sa astmom', 'SIRUP', 5, 'tamiflu.jpg', 500, 200, 3, 1);

INSERT INTO korisnik (korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, broj_telefona, datum_registracije, uloga, aktivan)
VALUES ('apotekar', 'apotekar', 'apotekar@mail.com', 'Apotekar', 'Prezime', '1990-01-01', 'Apotekarova adresa', 123456789, '2023-06-14 10:00:00', 'APOTEKAR', 1),
('pera', 'peralozinka', 'pera@email.com', 'Pera', 'Peric', '2000-01-01', 'Perina adresa', 111111111, '2022-01-01 10:00:00', 'KUPAC', 1),
('mika', 'mikalozinka', 'mika@email.com', 'Mika', 'Mikic', '1999-01-01', 'Mikina adresa', 222222222, '2021-12-01 10:00:00', 'ADMINISTRATOR', 1);

INSERT INTO loyalty_kartica (email_korisnika, popust, broj_poena, status) VALUES
('pera@email.com', '10%', 100, 'APPROVED'),
('mika@email.com', '15%', 150, 'APPROVED');

INSERT INTO komentar (tekst_komentara, ocena, datum_objavljivanja, autor_email, lek_id, anoniman) VALUES
('Odlican', 5, '2023-02-02', 'pera@email.com', 1, false);
INSERT INTO korpa (korisnik_email) VALUES ('pera@email.com');
INSERT INTO korpa (korisnik_email) VALUES ('mika@email.com');

INSERT INTO korpa_item (korpa_id, lek_id, kolicina) VALUES (1, 1, 3);
INSERT INTO korpa_item (korpa_id, lek_id, kolicina) VALUES (1, 2, 2);
INSERT INTO korpa_item (korpa_id, lek_id, kolicina) VALUES (2, 3, 1);

INSERT INTO nabavka_lekova (id, apotekar_email, datum_i_vreme, status, razlog_nabavke, razlog_odbijanja, lek_id, kolicina_nabavke)
VALUES
(1, 'apotekar@mail.com', '2023-06-14 12:00:00', 'PENDING', 'Nabavka za skladište', NULL, 1, 100);



