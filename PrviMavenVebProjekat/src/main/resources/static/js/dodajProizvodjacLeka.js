$(document).ready(function() {

	// keširanje referenci na elemente
	var nazivInput = $("input[name=naziv]")
	var drzavaSedisteInput = $("input[name=drzavaSediste]")
	
	function dodaj() {
		// čitanje parametara forme za dodavanje
		var naziv = nazivInput.val()
		var drzavaSediste = drzavaSedisteInput.val()
		// parametri zahteva
		var params = {
			naziv: naziv,
			drzavaSediste: drzavaSediste
		}
		console.log(params)
		$.post("proizvodjacLeka/add",
				params,
				function(){
					window.location.href = 'proizvodjacLeka'
				}
		)
		console.log("POST: " + "proizvodjacLeka/add")
		
		return false // sprečiti da submit forme promeni stranicu
	}
	
	$("form").submit(dodaj) // registracija handler-a za dodavanje
})