$(document).ready(function() {

	// keširanje referenci na elemente
	var tekstKomentaraInput = $("input[name=tekstKomentara]");
    var ocenaInput = $("input[name=ocena]");
    var datumObjavljivanjaStringInput = $("input[name=datumObjavljivanjaString]");
    var emailAutorInput = $("input[name=email_autor]");
    var lekIdInput = $("select[name=lek_id]");
    var anonimanInput = $("input[name=anoniman]");

	function dodaj() {
		// čitanje parametara forme za dodavanje
		var tekstKomentara = tekstKomentaraInput.val();
    var ocena = ocenaInput.val();
    var datumObjavljivanjaString = datumObjavljivanjaStringInput.val();
    var emailAutor = emailAutorInput.val();
    var lekId = lekIdInput.val();
    var anoniman = anonimanInput.is(":checked");
		// parametri zahteva
		var params = {
            tekstKomentara: tekstKomentara,
            ocena: ocena,
            datumObjavljivanjaString: datumObjavljivanjaString,
            emailAutor: emailAutor,
            lekId: lekId,
            anoniman: anoniman
            }
		console.log(params)
		$.post("komentar/add",
				params,
				function(){
					window.location.href = 'komentar'
				}
		)
		console.log("POST: " + "komentar/add")
		
		return false // sprečiti da submit forme promeni stranicu
	}
	
	$("form").submit(dodaj) // registracija handler-a za dodavanje
})