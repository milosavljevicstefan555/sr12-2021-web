$(document).ready(function() {
    var korisnikInput = $("input[name=email_korisnika]");
    var popustInput = $("input[name=popust]");
    var brojPoenaInput = $("input[name=brojPoena]");
    var statusInput = $("select[name=status]");
    
    javascript
    
    function dodajLoyaltyKarticu() {
        var email_korisnika = korisnikInput.val();
        var popust = popustInput.val();
        var brojPoena = brojPoenaInput.val();
        var status = statusInput.val();
        
        var params = {
            email_korisnika: email_korisnika,
            popust: popust,
            brojPoena: brojPoena,
            status: status
        }
        
        console.log(params);
        
        $.post("loyaltyKartica/add",
            params,
            function() {
                window.location.href = 'loyaltyKartica';
            }
        );
        
        console.log("POST: " + "loyaltyKartica/add");
        
        return false;
    }
    
    $("form").submit(dodajLoyaltyKarticu);
    
    });