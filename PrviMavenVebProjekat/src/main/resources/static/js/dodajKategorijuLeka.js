$(document).ready(function() {

	// keširanje referenci na elemente
	var nazivInput = $("input[name=naziv]")
	var namenaInput = $("input[name=namena]")
	var opisInput = $("input[name=opis]")
	
	function dodaj() {
		// čitanje parametara forme za dodavanje
		var naziv = nazivInput.val()
		var namena = namenaInput.val()
		var opis = opisInput.val()
		// parametri zahteva
		var params = {
			naziv: naziv,
			namena: namena,
			opis : opis
		}
		console.log(params)
		$.post("kategorijaLeka/add",
				params,
				function(){
					window.location.href = 'kategorijaLeka'
				}
		)
		console.log("POST: " + "kategorijaLeka/add")
		
		return false // sprečiti da submit forme promeni stranicu
	}
	
	$("form").submit(dodaj) // registracija handler-a za dodavanje
})