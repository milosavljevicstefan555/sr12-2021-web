package com.ftn.PrviMavenVebProjekat.model;

import java.util.ArrayList;
import java.util.List;

public class Korpa {
	private Long id;
	private Korisnik korisnik;
    private List<KorpaItem> items;

    
    
	public Korpa(Long id) {
		super();
		this.id = id;
	}

	public Korpa(Long id, Korisnik korisnik, List<KorpaItem> items) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.items = items;
	}

	public Korpa(Korisnik korisnik, List<KorpaItem> items) {
		super();
		this.korisnik = korisnik;
		this.items = items;
	}

	public Korpa() {
        this.items = new ArrayList<>();
    }

    public void dodajLek(Lek lek, int kolicina) {
        // Provera da li je lek već dodat u korpu
        for (KorpaItem item : items) {
            if (item.getLek().equals(lek)) {
                item.setKolicina(item.getKolicina() + kolicina);
                return;
            }
        }

        // Ako lek nije već dodat, dodajemo novi KorpaItem objekat
        KorpaItem item = new KorpaItem(lek, kolicina);
        items.add(item);
    }

    public void ukloniLek(Lek lek) {
        // Pronalazimo lek u korpi i uklanjamo ga
        for (KorpaItem item : items) {
            if (item.getLek().equals(lek)) {
                items.remove(item);
                return;
            }
        }
    }

    public void isprazniKorpu() {
        // Uklanjamo sve lekove iz korpe
        items.clear();
    }

    public double ukupnaCena() {
        // Računamo ukupnu cenu svih lekova u korpi
        double ukupnaCena = 0;
        for (KorpaItem item : items) {
            ukupnaCena += item.getLek().getCena() * item.getKolicina();
        }
        return ukupnaCena;
    }

    public void prikaziKorpu() {
        // Prikazujemo sadržaj korpe
        for (KorpaItem item : items) {
            System.out.println("Lek: " + item.getLek().getNaziv() + ", Količina: " + item.getKolicina());
        }
    }
    

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public List<KorpaItem> getItems() {
		return items;
	}

	public void setItems(List<KorpaItem> items) {
		this.items = items;
	}
    
}
