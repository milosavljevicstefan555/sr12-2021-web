package com.ftn.PrviMavenVebProjekat.model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Racun {
    private Long id;
    private Korisnik kupac;
    private List<RacunItem> lekovi;
    private double cena;
    private Date timeStamp;
    
    public Racun() {
    	lekovi = new ArrayList<RacunItem>();
	}

	public Racun(Korisnik kupac, List<RacunItem> lekovi, Date timeStamp) {
		super();
		this.kupac = kupac;
		this.lekovi = lekovi;
		this.cena = calculateCena();
		this.timeStamp = timeStamp;
	}

	public Racun(Long id, Korisnik kupac, List<RacunItem> lekovi, Date timeStamp) {
        this.id = id;
        this.kupac = kupac;
        this.lekovi = lekovi;
        this.timeStamp = timeStamp;
        this.cena = calculateCena(); // Automatsko racunanje cene
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Korisnik getKupac() {
        return kupac;
    }

    public void setKupac(Korisnik kupac) {
        this.kupac = kupac;
    }

    public List<RacunItem> getLekovi() {
        return lekovi;
    }

    public void setLekovi(List<RacunItem> lekovi) {
        this.lekovi = lekovi;
    }

    public double getCena() {
        return cena;
    }

    
    public void setCena(double cena) {
		this.cena = cena;
	}

	public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    // Metoda za racunanje cene racuna
    private double calculateCena() {
        double total = 0.0;
        for (RacunItem item : lekovi) {
            total += item.getLek().getCena() * item.getKolicina();
        }
        return total;
    }
}
