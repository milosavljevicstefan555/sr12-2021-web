package com.ftn.PrviMavenVebProjekat.model;

import java.time.LocalDateTime;

import com.ftn.PrviMavenVebProjekat.bean.StatusLoyaltyEnum;

public class NabavkaLekova {
    private long id;
    private Korisnik apotekar;
    private LocalDateTime datumIVreme;
    private StatusLoyaltyEnum status;
    private String razlogNabavke;
    private String razlogOdbijanja;
    private Lek lek;
    private int kolicinaNabavke;

    public NabavkaLekova() {
    }

    public NabavkaLekova(Korisnik apotekar, LocalDateTime datumIVreme, StatusLoyaltyEnum status, String razlogNabavke, Lek lek, int kolicinaNabavke) {
        this.apotekar = apotekar;
        this.datumIVreme = datumIVreme;
        this.status = status;
        this.razlogNabavke = razlogNabavke;
        this.lek = lek;
        this.kolicinaNabavke = kolicinaNabavke;
    }
    
    

    // Getters and setters

    public NabavkaLekova(long id, Korisnik apotekar, LocalDateTime datumIVreme, StatusLoyaltyEnum status,
			String razlogNabavke, String razlogOdbijanja, Lek lek, int kolicinaNabavke) {
		super();
		this.id = id;
		this.apotekar = apotekar;
		this.datumIVreme = datumIVreme;
		this.status = status;
		this.razlogNabavke = razlogNabavke;
		this.razlogOdbijanja = razlogOdbijanja;
		this.lek = lek;
		this.kolicinaNabavke = kolicinaNabavke;
	}

	public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Korisnik getApotekar() {
        return apotekar;
    }

    public void setApotekar(Korisnik apotekar) {
        this.apotekar = apotekar;
    }

    public LocalDateTime getDatumIVreme() {
        return datumIVreme;
    }

    public void setDatumIVreme(LocalDateTime datumIVreme) {
        this.datumIVreme = datumIVreme;
    }

    public StatusLoyaltyEnum getStatus() {
        return status;
    }

    public void setStatus(StatusLoyaltyEnum status) {
        this.status = status;
    }

    public String getRazlogNabavke() {
        return razlogNabavke;
    }

    public void setRazlogNabavke(String razlogNabavke) {
        this.razlogNabavke = razlogNabavke;
    }

    public String getRazlogOdbijanja() {
        return razlogOdbijanja;
    }

    public void setRazlogOdbijanja(String razlogOdbijanja) {
        this.razlogOdbijanja = razlogOdbijanja;
    }

    public Lek getLek() {
        return lek;
    }

    public void setLek(Lek lek) {
        this.lek = lek;
    }

    public int getKolicinaNabavke() {
        return kolicinaNabavke;
    }

    public void setKolicinaNabavke(int kolicinaNabavke) {
        this.kolicinaNabavke = kolicinaNabavke;
    }
}
