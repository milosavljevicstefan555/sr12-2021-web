package com.ftn.PrviMavenVebProjekat.model;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.ftn.PrviMavenVebProjekat.bean.UlogaEnum;

public class Korisnik {
	private String korisnickoIme;
	private String loznka;
	private String email;
	private String ime;
	private String prezime;
	private Date datumRodjenja;
	private String adresa;
	private String brojTelefona;
	private LocalDateTime vremeRegistracije;
	private UlogaEnum ulogaKorisnika;
	private boolean aktivan;
	
	public Korisnik(String korisnickoIme, String loznka, String email, String ime, String prezime, Date datumRodjenja,
			String adresa, String brojTelefona, LocalDateTime vremeRegistracije, UlogaEnum ulogaKorisnika) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.loznka = loznka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.adresa = adresa;
		this.brojTelefona = brojTelefona;
		this.vremeRegistracije = vremeRegistracije;
		this.ulogaKorisnika = ulogaKorisnika;
	}

	public Korisnik() {
		super();
	}

	public Korisnik(String korisnickoIme, String loznka, String email, String ime, String prezime, Date datumRodjenja,
			String adresa, String brojTelefona, LocalDateTime vremeRegistracije, UlogaEnum ulogaKorisnika,
			boolean aktivan) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.loznka = loznka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = datumRodjenja;
		this.adresa = adresa;
		this.brojTelefona = brojTelefona;
		this.vremeRegistracije = vremeRegistracije;
		this.ulogaKorisnika = ulogaKorisnika;
		this.aktivan = aktivan;
	}

	@Override
	public String toString() {
		String pattern = "dd/MM/yyyy HH:mm:ss";
		DateFormat df = new SimpleDateFormat(pattern);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		return korisnickoIme + ";" + loznka + ";" + email + ";" + ime + ";" + prezime + ";" + df.format(datumRodjenja) + ";"
				+ adresa + ";" + brojTelefona + ";" + vremeRegistracije.format(formatter)+ ";" + ulogaKorisnika;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLoznka() {
		return loznka;
	}

	public void setLoznka(String loznka) {
		this.loznka = loznka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Date getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public LocalDateTime getVremeRegistracije() {
		return vremeRegistracije;
	}

	public void setVremeRegistracije(LocalDateTime vremeRegistracije) {
		this.vremeRegistracije = vremeRegistracije;
	}

	public UlogaEnum getUlogaKorisnika() {
		return ulogaKorisnika;
	}

	public void setUlogaKorisnika(UlogaEnum ulogaKorisnika) {
		this.ulogaKorisnika = ulogaKorisnika;
	}

	public boolean isAktivan() {
		return aktivan;
	}

	public void setAktivan(boolean aktivan) {
		this.aktivan = aktivan;
	}
	
	
	
}
