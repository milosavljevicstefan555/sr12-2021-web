package com.ftn.PrviMavenVebProjekat.model;

import java.util.Date;
import java.util.Objects;

public class Komentar {
	
	private Long id;
	private String tekstKomentara;
	private int ocena;
	private Date datumObjavljivanja;
	private Korisnik autor;
	private Lek lek;
	private boolean anoniman;
	
	public Komentar() {
		super();
	}

	public Komentar(String tekstKomentara, int ocena, Date datumObjavljivanja, Korisnik autor, Lek lek,
			boolean anoniman) {
		super();
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumObjavljivanja = datumObjavljivanja;
		this.autor = autor;
		this.lek = lek;
		this.anoniman = anoniman;
	}

	public Komentar(Long id, String tekstKomentara, int ocena, Date datumObjavljivanja, Korisnik autor, Lek lek,
			boolean anoniman) {
		super();
		this.id = id;
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.datumObjavljivanja = datumObjavljivanja;
		this.autor = autor;
		this.lek = lek;
		this.anoniman = anoniman;
	}

	@Override
	public int hashCode() {
		return Objects.hash(anoniman, autor, datumObjavljivanja, id, lek, ocena, tekstKomentara);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Komentar other = (Komentar) obj;
		return anoniman == other.anoniman && Objects.equals(autor, other.autor)
				&& Objects.equals(datumObjavljivanja, other.datumObjavljivanja) && Objects.equals(id, other.id)
				&& Objects.equals(lek, other.lek) && ocena == other.ocena
				&& Objects.equals(tekstKomentara, other.tekstKomentara);
	}

	@Override
	public String toString() {
		return "Komentar [id=" + id + ", tekstKomentara=" + tekstKomentara + ", ocena=" + ocena
				+ ", datumObjavljivanja=" + datumObjavljivanja + ", autor=" + autor + ", lek=" + lek + ", anoniman="
				+ anoniman + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTekstKomentara() {
		return tekstKomentara;
	}

	public void setTekstKomentara(String tekstKomentara) {
		this.tekstKomentara = tekstKomentara;
	}

	public int getOcena() {
		return ocena;
	}

	public void setOcena(int ocena) {
		this.ocena = ocena;
	}

	public Date getDatumObjavljivanja() {
		return datumObjavljivanja;
	}

	public void setDatumObjavljivanja(Date datumObjavljivanja) {
		this.datumObjavljivanja = datumObjavljivanja;
	}

	public Korisnik getAutor() {
		return autor;
	}

	public void setAutor(Korisnik autor) {
		this.autor = autor;
	}

	public Lek getLek() {
		return lek;
	}

	public void setLek(Lek lek) {
		this.lek = lek;
	}

	public boolean isAnoniman() {
		return anoniman;
	}

	public void setAnoniman(boolean anoniman) {
		this.anoniman = anoniman;
	}

	
	
	

}
