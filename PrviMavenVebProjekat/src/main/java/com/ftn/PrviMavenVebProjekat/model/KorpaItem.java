package com.ftn.PrviMavenVebProjekat.model;

public class KorpaItem {
	 private Long id;
	 private Lek lek;
	 private int kolicina;

	 
	 public KorpaItem() {
		super();
	}

	public KorpaItem(Long id, Lek lek, int kolicina) {
		super();
		this.id = id;
		this.lek = lek;
		this.kolicina = kolicina;
	}

	public KorpaItem(Lek lek, int kolicina) {
	      this.lek = lek;
	      this.kolicina = kolicina;
	  }

	
	  public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLek(Lek lek) {
		this.lek = lek;
	}

	public Lek getLek() {
	      return lek;
	  }

	  public int getKolicina() {
	      return kolicina;
	  }

	  public void setKolicina(int kolicina) {
	      this.kolicina = kolicina;
   	  }
}
