package com.ftn.PrviMavenVebProjekat.model;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KategorijeLekova {
	
	private Map<Long, KategorijaLeka> kategorijeLekova = new HashMap<>();
	private long nextId = 1L;

	
	public KategorijeLekova() {

		try {
			Path path = Paths.get(getClass().getClassLoader().getResource("kategorijeLekova.txt").toURI());
			System.out.println(path.toFile().getAbsolutePath());
			List<String> lines = Files.readAllLines(path, Charset.forName("UTF-8"));

			for (String line : lines) {
				line = line.trim();
				if (line.equals("") || line.indexOf('#') == 0)
					continue;
				
				String[] tokens = line.split(";");
				Long id = Long.parseLong(tokens[0]);
				
				if(nextId<id)
					nextId=id;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/** vraca knjigu u odnosu na zadati id */
	public KategorijaLeka findOne(Long id) {
		return kategorijeLekova.get(id);
	}

	/** vraca sve kategorijaLekova */
	public List<KategorijaLeka> findAll() {
		return new ArrayList<KategorijaLeka>(kategorijeLekova.values());
	}

	/** cuva podatke o knjizi */
	public KategorijaLeka save(KategorijaLeka knjiga) {
		if (knjiga.getId() == null) {
			knjiga.setId(++nextId);
		}
		kategorijeLekova.put(knjiga.getId(), knjiga);
		return knjiga;
	}

	/** cuva podatke o svim knjigama */
	public List<KategorijaLeka> save(List<KategorijaLeka> kategorijaLekova) {
		List<KategorijaLeka> ret = new ArrayList<>();

		for (KategorijaLeka k : kategorijaLekova) {
			// za svaku prosleđenu knjigu pozivamo save
			// metodu koju smo već napravili i testirali -
			// ona će sepobrinuti i za dodelu ID-eva
			// ako je to potrebno
			KategorijaLeka saved = save(k);

			// uspešno snimljene dodajemo u listu za vraćanje
			if (saved != null) {
				ret.add(saved);
			}
		}
		return ret;
	}

	/** brise knjigu u odnosu na zadati id */
	public KategorijaLeka delete(Long id) {
		if (!kategorijeLekova.containsKey(id)) {
			throw new IllegalArgumentException("tried to remove non existing book");
		}
		KategorijaLeka knjiga = kategorijeLekova.get(id);
		if (knjiga != null) {
			kategorijeLekova.remove(id);
		}
		return knjiga;
	}

	/** brise kategorijaLekova u odnosu na zadate ids */
	public void delete(List<Long> ids) {
		for (Long id : ids) {
			// pozivamo postojeću metodu za svaki
			delete(id);
		}
	}

	/** vraca sve kategorijaLekova ciji naziv zapocinje sa tekstom*/
	public List<KategorijaLeka> findByNaziv(String naziv) {
		List<KategorijaLeka> ret = new ArrayList<>();

		for (KategorijaLeka k : kategorijeLekova.values()) {
			if (naziv.startsWith(k.getNaziv())) {
				ret.add(k);
			}
		}

		return ret;
	}
}
