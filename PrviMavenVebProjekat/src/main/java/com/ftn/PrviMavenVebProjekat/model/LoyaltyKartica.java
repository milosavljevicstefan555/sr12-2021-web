package com.ftn.PrviMavenVebProjekat.model;

import java.util.Objects;

import com.ftn.PrviMavenVebProjekat.bean.StatusLoyaltyEnum;

public class LoyaltyKartica {
	
	private Korisnik korisnik; 
	private String popust;
	private int brojPoena;
	private StatusLoyaltyEnum status;
	
	private static final int POINTS_PER_AMOUNT = 500;
	private static final int DISCOUNT_PER_POINT = 5;
	private static final int MAX_POINTS_PER_PURCHASE = 10;
	
	public LoyaltyKartica() {
		super();
	}

	public LoyaltyKartica(String popust, int brojPoena, StatusLoyaltyEnum status) {
		super();
		this.popust = popust;
		this.brojPoena = brojPoena;
		this.status = status;
	}

	public LoyaltyKartica(Korisnik korisnik, String popust, int brojPoena, StatusLoyaltyEnum status) {
		super();
		this.korisnik = korisnik;
		this.popust = popust;
		this.brojPoena = brojPoena;
		this.status = status;
	}

	@Override
	public int hashCode() {
		return Objects.hash(brojPoena, korisnik, popust, status);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LoyaltyKartica other = (LoyaltyKartica) obj;
		return brojPoena == other.brojPoena && Objects.equals(korisnik, other.korisnik)
				&& Objects.equals(popust, other.popust) && status == other.status;
	}

	@Override
	public String toString() {
		return "LoyaltyKartica [korisnik=" + korisnik + ", popust=" + popust + ", brojPoena=" + brojPoena + ", status="
				+ status + "]";
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public String getPopust() {
		return popust;
	}

	public void setPopust(String popust) {
		this.popust = popust;
	}

	public int getBrojPoena() {
		return brojPoena;
	}

	public void setBrojPoena(int brojPoena) {
		this.brojPoena = brojPoena;
	}

	public StatusLoyaltyEnum getStatus() {
		return status;
	}

	public void setStatus(StatusLoyaltyEnum status) {
		this.status = status;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	
	public void addPoints(double amountSpent) {
		int earnedPoints = (int) (amountSpent / POINTS_PER_AMOUNT);
		brojPoena += earnedPoints;
	}
	
	public double calculateDiscount(int usedPoints) {
		if (usedPoints > brojPoena) {
			usedPoints = brojPoena;
		}
		if (usedPoints > MAX_POINTS_PER_PURCHASE) {
			usedPoints = MAX_POINTS_PER_PURCHASE;
		}
		
		double discount = usedPoints * DISCOUNT_PER_POINT;
		brojPoena -= usedPoints;
		
		return discount;
	}
}
