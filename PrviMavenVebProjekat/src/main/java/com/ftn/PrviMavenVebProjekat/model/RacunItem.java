package com.ftn.PrviMavenVebProjekat.model;

public class RacunItem {
	private Long id;
	private Lek lek;
	private int kolicina;
	 
	public RacunItem() {
		super();
	}

	public RacunItem(Lek lek, int kolicina) {
		super();
		this.lek = lek;
		this.kolicina = kolicina;
	}

	public RacunItem(Long id, Lek lek, int kolicina) {
		super();
		this.id = id;
		this.lek = lek;
		this.kolicina = kolicina;
	}

	@Override
	public String toString() {
		return "RacunItem [id=" + id + ", lek=" + lek + ", kolicina=" + kolicina + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Lek getLek() {
		return lek;
	}

	public void setLek(Lek lek) {
		this.lek = lek;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}
	

}
