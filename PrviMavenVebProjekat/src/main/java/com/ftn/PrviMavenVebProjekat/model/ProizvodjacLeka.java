package com.ftn.PrviMavenVebProjekat.model;

public class ProizvodjacLeka {

	private Long id;
	private String naziv;
	private String drzavaSediste;
	
	@Override
	public String toString() {
		return "ProizvodjacLeka [id=" + id + ", naziv=" + naziv + ", drzavaSediste=" + drzavaSediste + "]";
	}

	public ProizvodjacLeka() {
		super();
	}

	public ProizvodjacLeka(Long id, String naziv, String drzavaSediste) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.drzavaSediste = drzavaSediste;
	}

	public ProizvodjacLeka(String naziv, String drzavaSediste) {
		super();
		this.naziv = naziv;
		this.drzavaSediste = drzavaSediste;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getDrzavaSediste() {
		return drzavaSediste;
	}

	public void setDrzavaSediste(String drzavaSediste) {
		this.drzavaSediste = drzavaSediste;
	}
	
	
	
}
