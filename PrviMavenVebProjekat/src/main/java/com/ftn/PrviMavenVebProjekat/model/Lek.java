package com.ftn.PrviMavenVebProjekat.model;

import com.ftn.PrviMavenVebProjekat.bean.OblikEnum;

public class Lek {

	private Long id;
	private String naziv;
	private String opis;
	private String kontraindikacije;
	private OblikEnum oblik;
	private double prosecnaOcena;
	private String slikaURL;
	private int dostupnaKolicina;
	private double cena;
	private ProizvodjacLeka proizvodjac;
	private KategorijaLeka kategorija;
	
	public Lek() {
		super();
	}

	public Lek(String naziv, String opis, String kontraindikacije, OblikEnum oblik, double prosecnaOcena,
			String slikaURL, int dostupnaKolicina, double cena, ProizvodjacLeka proizvodjac, KategorijaLeka kategorija) {
		super();
		this.naziv = naziv;
		this.opis = opis;
		this.kontraindikacije = kontraindikacije;
		this.oblik = oblik;
		this.prosecnaOcena = prosecnaOcena;
		this.slikaURL = slikaURL;
		this.dostupnaKolicina = dostupnaKolicina;
		this.cena = cena;
		this.proizvodjac = proizvodjac;
		this.kategorija = kategorija;
	}

	public Lek(Long id, String naziv, String opis, String kontraindikacije, OblikEnum oblik, double prosecnaOcena,
			String slikaURL, int dostupnaKolicina, double cena, ProizvodjacLeka proizvodjac, KategorijaLeka kategorija) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.kontraindikacije = kontraindikacije;
		this.oblik = oblik;
		this.prosecnaOcena = prosecnaOcena;
		this.slikaURL = slikaURL;
		this.dostupnaKolicina = dostupnaKolicina;
		this.cena = cena;
		this.proizvodjac = proizvodjac;
		this.kategorija = kategorija; 
	}

	@Override
	public String toString() {
		return "Lek [id=" + id + ", naziv=" + naziv + ", opis=" + opis + ", kontraindikacije=" + kontraindikacije
				+ ", oblik=" + oblik + ", prosecnaOcena=" + prosecnaOcena + ", slikaURL=" + slikaURL
				+ ", dostupnaKolicina=" + dostupnaKolicina + ", cena=" + cena + ", proizvodjac=" + proizvodjac + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getKontraindikacije() {
		return kontraindikacije;
	}

	public void setKontraindikacije(String kontraindikacije) {
		this.kontraindikacije = kontraindikacije;
	}

	public OblikEnum getOblik() {
		return oblik;
	}

	public void setOblik(OblikEnum oblik) {
		this.oblik = oblik;
	}

	public double getProsecnaOcena() {
		return prosecnaOcena;
	}

	public void setProsecnaOcena(double prosecnaOcena) {
		this.prosecnaOcena = prosecnaOcena;
	}

	public String getSlikaURL() {
		return slikaURL;
	}

	public void setSlikaURL(String slikaURL) {
		this.slikaURL = slikaURL;
	}

	public int getDostupnaKolicina() {
		return dostupnaKolicina;
	}

	public void setDostupnaKolicina(int dostupnaKolicina) {
		this.dostupnaKolicina = dostupnaKolicina;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public ProizvodjacLeka getProizvodjac() {
		return proizvodjac;
	}

	public void setProizvodjac(ProizvodjacLeka proizvodjac) {
		this.proizvodjac = proizvodjac;
	}

	public KategorijaLeka getKategorija() {
		return kategorija;
	}

	public void setKategorija(KategorijaLeka kategorija) {
		this.kategorija = kategorija;
	}
	
	
}
