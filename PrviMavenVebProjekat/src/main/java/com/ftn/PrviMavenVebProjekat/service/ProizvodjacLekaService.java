package com.ftn.PrviMavenVebProjekat.service;

import java.util.ArrayList;

import com.ftn.PrviMavenVebProjekat.model.ProizvodjacLeka;

public interface ProizvodjacLekaService {
	ProizvodjacLeka findOne(Long id);
	ArrayList<ProizvodjacLeka> findAll();
	ProizvodjacLeka save(ProizvodjacLeka pLeka);
	ProizvodjacLeka update(ProizvodjacLeka pLeka);
	ProizvodjacLeka delete(Long id);
}
