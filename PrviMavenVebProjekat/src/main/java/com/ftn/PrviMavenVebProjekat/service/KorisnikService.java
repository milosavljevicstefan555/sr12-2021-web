package com.ftn.PrviMavenVebProjekat.service;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;


public interface KorisnikService {
	
	Korisnik findOneById(String korisnickoIme);
	Korisnik findOne(String email); 
	Korisnik findOne(String email, String sifra);
	List<Korisnik> findAll(); 
	Korisnik save(Korisnik korisnik); 
	Korisnik update(Korisnik korisnik); 
	Korisnik delete(String korisnickoIme); 
	List<Korisnik> findAllNeodobrene();
	void approveUser(Korisnik korisnik);
	void denyUser(Korisnik korisnik);

}
