package com.ftn.PrviMavenVebProjekat.service;

import java.util.ArrayList;

import com.ftn.PrviMavenVebProjekat.model.Komentar;

public interface KomentarService {
	Komentar findOne(Long id);
	ArrayList<Komentar> findAll();
	Komentar save(Komentar komentar);
	Komentar update(Komentar komentar);
	Komentar delete(Long id);
	double calculateAverageRatingForLek(Long lekId);
}
