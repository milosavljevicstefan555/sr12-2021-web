package com.ftn.PrviMavenVebProjekat.service;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.KorpaItem;

public interface KorpaItemService {
    KorpaItem findOne(Long id);
    List<KorpaItem> findAll();
    KorpaItem save(KorpaItem korpaItem, Long korpa_id);
    KorpaItem update(KorpaItem korpaItem);
    KorpaItem delete(Long id);
}
