package com.ftn.PrviMavenVebProjekat.service;

import java.util.ArrayList;

import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;

public interface KategorijaLekaService {
	KategorijaLeka findOne (Long id);
	ArrayList<KategorijaLeka> findAll();
	KategorijaLeka save(KategorijaLeka kLeka);
	KategorijaLeka update(KategorijaLeka kLeka);
	KategorijaLeka delete(Long id);
}
