package com.ftn.PrviMavenVebProjekat.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Korpa;
import com.ftn.PrviMavenVebProjekat.model.KorpaItem;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.KorpaItemService;
import com.ftn.PrviMavenVebProjekat.service.KorpaService;
import com.mysql.cj.xdevapi.SqlMultiResult;

@Repository
public class KorpaServiceImpl implements KorpaService {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private KorisnikService korisnikService;
    @Autowired
    private KorpaItemService korpaItemService;
    
    private class KorpaRowCallbackHandler implements RowCallbackHandler {
        
        private List<Korpa> korpe = new ArrayList<>();
        
        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            Korisnik korisnik = korisnikService.findOne(resultSet.getString(index++));
            KorpaItem korpaItem = korpaItemService.findOne(resultSet.getLong(index++));
            // Retrieve other attributes as needed from the ResultSet
            Korpa korpa = null;
            for (Korpa k : korpe) {
            	if(k.getKorisnik().getEmail().equals(korisnik.getEmail())) {
            		korpa = k;
            	}
            }
            if (korpa == null) {
            	korpa = new Korpa();
            	korpa.setId(id);
            	korpa.setKorisnik(korisnik);
            	 if (korpaItem != null) {
             		List<KorpaItem> items = korpa.getItems();
                     items.add(korpaItem);
                     korpa.setItems(items);
             	}
                korpe.add(korpa);
            	return;
            } else {
            	 if (korpaItem != null) {
             		List<KorpaItem> items = korpa.getItems();
                     items.add(korpaItem);
                     korpa.setItems(items);
             	}
            }
            
        }
        
        private List<Korpa> getKorpe() {
            return korpe;
        }
    }
    
    public Korpa findOne(Long id) {
        String sql = "SELECT k.id AS korpa_id, k.korisnik_email, ki.id AS korpa_item_id, ki.lek_id, ki.kolicina\r\n"
        		+ "FROM korpa k\r\n"
        		+ "left JOIN korpa_item ki ON k.id = ki.korpa_id\r\n"
        		+ "WHERE k.id = ?;";
        KorpaRowCallbackHandler rowCallbackHandler = new KorpaRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        List<Korpa> korpe = rowCallbackHandler.getKorpe();
        return korpe.isEmpty() ? null : korpe.get(0);
    }
    
    public List<Korpa> findAll() {
        String sql = "SELECT k.id AS korpa_id, k.korisnik_email, ki.id AS korpa_item_id, ki.lek_id, ki.kolicina\r\n"
        		+ "FROM korpa k\r\n"
        		+ "left JOIN korpa_item ki ON k.id = ki.korpa_id;";
        KorpaRowCallbackHandler rowCallbackHandler = new KorpaRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getKorpe();
    }
    
    @Transactional
    public Korpa save(Korpa korpa) {
		PreparedStatementCreator statementCreator = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO korpa (korisnik_email) VALUES (?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, korpa.getKorisnik().getEmail());
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(statementCreator, keyHolder) == 1;
		return uspeh?korpa:null;
    }
    
    @Transactional
    public Korpa update(Korpa korpa) {
        String sql = "UPDATE korpa SET korisnik_email = ? WHERE id = ?";
        boolean rowsAffected = jdbcTemplate.update(sql, korpa.getKorisnik().getEmail(), korpa.getId()) == 1;
        return rowsAffected? korpa : null;
    }
    
    @Transactional
    public Korpa delete(Long id) {
    	deleteKorpaItems(id);
        String sql = "DELETE FROM korpa WHERE id = ?";
        
        jdbcTemplate.update(sql, id);
        
        return null;
    }
    
    private void deleteKorpaItems(Long id) {
    	String sql= "DELETE FROM korpa_item WHERE korpa_id = ?";
    	jdbcTemplate.update(sql, id);
    	return;
    }
}
