package com.ftn.PrviMavenVebProjekat.service;
import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Korpa;


public interface KorpaService {
    Korpa findOne(Long id);
    List<Korpa> findAll();
    Korpa save(Korpa korpa);
    Korpa update(Korpa korpa);
    Korpa delete(Long id);
}
