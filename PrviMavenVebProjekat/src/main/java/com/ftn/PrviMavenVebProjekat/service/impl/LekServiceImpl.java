package com.ftn.PrviMavenVebProjekat.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.LekDAO;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.service.LekService;

@Service
public class LekServiceImpl implements LekService {

	@Autowired
	private LekDAO lekDao;
	
	@Override
	public Lek findOne(Long id) {
		return lekDao.findOne(id);
	}

	@Override
	public ArrayList<Lek> findAll() {
		return lekDao.findAll();
	}

	@Override
	public Lek save(Lek lek) {
		return lekDao.save(lek);
	}

	@Override
	public Lek update(Lek lek) {
		return lekDao.update(lek);
	}

	@Override
	public Lek delete(Long id) {
		return lekDao.delete(id);
	}

	@Override
	public List<Lek> searchLekovi(String searchTerm) {
		return lekDao.searchLekovi(searchTerm);
	}

}
