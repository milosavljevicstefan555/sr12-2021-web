package com.ftn.PrviMavenVebProjekat.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.KategorijaLekaDAO;
import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka; 
import com.ftn.PrviMavenVebProjekat.service.KategorijaLekaService;


@Service
public class KategorijaLekaServiceImpl implements KategorijaLekaService {
	
	@Autowired
	private KategorijaLekaDAO kategorijaLekaDAO;

	@Override
	public KategorijaLeka findOne(Long id) {
		return kategorijaLekaDAO.findOne(id);
	}

	@Override
	public ArrayList<KategorijaLeka> findAll() {
		return kategorijaLekaDAO.findAll();
	}

	@Override
	public KategorijaLeka save(KategorijaLeka kLeka) {
		return kategorijaLekaDAO.save(kLeka);
	}

	@Override
	public KategorijaLeka update(KategorijaLeka kLeka) {
		return kategorijaLekaDAO.update(kLeka);
	}

	@Override
	public KategorijaLeka delete(Long id) {
		return kategorijaLekaDAO.delete(id);
	}
	

}
