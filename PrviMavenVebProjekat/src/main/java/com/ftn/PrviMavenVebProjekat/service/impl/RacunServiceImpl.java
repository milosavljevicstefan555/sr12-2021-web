package com.ftn.PrviMavenVebProjekat.service.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.KorpaItem;
import com.ftn.PrviMavenVebProjekat.model.Racun;
import com.ftn.PrviMavenVebProjekat.model.RacunItem;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.RacunItemService;
import com.ftn.PrviMavenVebProjekat.service.RacunService;

@Repository
public class RacunServiceImpl implements RacunService {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private KorisnikService korisnikService;
    @Autowired
    private RacunItemService racunItemService;
    

    private class RacunRowCallbackHandler implements RowCallbackHandler {

        private List<Racun> racuni = new ArrayList<>();

        @Override
        public void processRow(ResultSet rs) throws SQLException {
            int index = 1;
            Long id = rs.getLong(index++);
            String korisnikEmail = rs.getString(index++);
            Long racunItemId = rs.getLong(index++);
            Long lekId = rs.getLong(index++);
            int kolicina = rs.getInt(index++);
            Date timeStamp = rs.getDate(index++);
            double cena = rs.getDouble(index++);

            Racun racun = null;
            for (Racun r : racuni) {
                if (r.getId().equals(id)) {
                    racun = r;
                    break;
                }
            }

            if (racun == null) {
                racun = new Racun();
                racun.setId(id);
                racun.setKupac(korisnikService.findOne(korisnikEmail));
                racun.setTimeStamp(timeStamp);
                racun.setCena(cena);
                racuni.add(racun);
            }

            RacunItem racunItem = racunItemService.findOne(racunItemId);
            if (racunItem != null) {
                racun.getLekovi().add(racunItem);
            }
        }

        private List<Racun> getRacuni() {
            return racuni;
        }

    }

    public Racun findOne(Long id) {
        String sql = "SELECT r.id, r.korisnik_email, ri.id AS racun_item_id, ri.lek_id, ri.kolicina, r.timestamp, r.cena "
                + "FROM racun r "
                + "left JOIN racun_item ri ON r.id = ri.racun_id "
                + "WHERE r.id = ?";
        RacunRowCallbackHandler rowCallbackHandler = new RacunRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        List<Racun> racuni = rowCallbackHandler.getRacuni();
        return racuni.isEmpty() ? null : racuni.get(0);
    }

    public List<Racun> findAll() {
        String sql = "SELECT r.id, r.korisnik_email, ri.id AS racun_item_id, ri.lek_id, ri.kolicina, r.timestamp, r.cena "
                + "FROM racun r "
                + "left JOIN racun_item ri ON r.id = ri.racun_id";
        RacunRowCallbackHandler rowCallbackHandler = new RacunRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getRacuni();
    }

    @Transactional
    public Racun save(Racun racun) {
        PreparedStatementCreator statementCreator = con -> {
            String sql = "INSERT INTO racun (korisnik_email, timestamp, cena) VALUES (?, ?, ?)";
            PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            int index = 1;
            preparedStatement.setString(index++, racun.getKupac().getEmail());
            preparedStatement.setDate(index++, racun.getTimeStamp());
            preparedStatement.setDouble(index++, racun.getCena());
            return preparedStatement;
        };
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean success = jdbcTemplate.update(statementCreator, keyHolder) == 1;
        if (success) {
            Long racunId = keyHolder.getKey().longValue();
            for (RacunItem racunItem : racun.getLekovi()) {
                saveRacunItem(racunId, racunItem);
            }
            racun.setId(racunId);
            return racun;
        } else {
            return null;
        }
    }

    @Transactional
    public Racun update(Racun racun) {
        String sql = "UPDATE racun SET korisnik_email = ?, timestamp = ?, cena = ? WHERE id = ?";
        boolean rowsAffected = jdbcTemplate.update(sql, racun.getKupac().getEmail(), racun.getTimeStamp(),
                racun.getCena(), racun.getId()) == 1;
        if (rowsAffected) {
            deleteRacunItems(racun.getId());
            for (RacunItem racunItem : racun.getLekovi()) {
                saveRacunItem(racun.getId(), racunItem);
            }
            return racun;
        } else {
            return null;
        }
    }

    @Transactional
    public void delete(Long id) {
        deleteRacunItems(id);
        String sql = "DELETE FROM racun WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }

    private void saveRacunItem(Long racunId, RacunItem racunItem) {
        String sql = "INSERT INTO racun_item (racun_id, lek_id, kolicina) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, racunId, racunItem.getLek().getId(), racunItem.getKolicina());
    }

    private void deleteRacunItems(Long racunId) {
        String sql = "DELETE FROM racun_item WHERE racun_id = ?";
        jdbcTemplate.update(sql, racunId);
    }

	@Override
	public List<Racun> findPurchaseHistoryForUser(Korisnik korisnik) {
		String sql = "SELECT id, korisnik_email, cena, timestamp FROM racun WHERE korisnik_email = ?";
	    RowMapper<Racun> rowMapper = new BeanPropertyRowMapper<>(Racun.class);
	    return jdbcTemplate.query(sql, rowMapper, korisnik.getEmail());
	}
	
}
