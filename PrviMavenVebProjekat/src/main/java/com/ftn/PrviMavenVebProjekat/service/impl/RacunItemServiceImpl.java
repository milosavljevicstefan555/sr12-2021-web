package com.ftn.PrviMavenVebProjekat.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.model.RacunItem;
import com.ftn.PrviMavenVebProjekat.service.LekService;
import com.ftn.PrviMavenVebProjekat.service.RacunItemService;

@Repository
public class RacunItemServiceImpl implements RacunItemService {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private LekService lekService;
    
    private class RacunItemRowCallbackHandler implements RowCallbackHandler {
        
        private List<RacunItem> racunItems = new ArrayList<>();
        
        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            Lek lek = lekService.findOne(resultSet.getLong(index++));
            int kolicina = resultSet.getInt(index++);
            
            RacunItem racunItem = new RacunItem(id, lek, kolicina);
            
            racunItems.add(racunItem);
        }
        
        private List<RacunItem> getRacunItems() {
            return racunItems;
        }
    }
    
    public RacunItem findOne(Long id) {
        String sql = "SELECT id, lek_id, kolicina FROM racun_item WHERE id = ?";
        RacunItemRowCallbackHandler rowCallbackHandler = new RacunItemRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        List<RacunItem> racunItems = rowCallbackHandler.getRacunItems();
        return racunItems.isEmpty() ? null : racunItems.get(0);
    }
    
    public List<RacunItem> findAll() {
        String sql = "SELECT id, lek_id, kolicina FROM racun_item";
        RacunItemRowCallbackHandler rowCallbackHandler = new RacunItemRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getRacunItems();
    }
    
    @Transactional
    public RacunItem save(RacunItem racunItem, Long racunId) {
        PreparedStatementCreator statementCreator = new PreparedStatementCreator() {
            
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                String sql = "INSERT INTO racun_item(racun_id, lek_id, kolicina) VALUES (?, ?, ?)";
                PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                int index = 1;
                preparedStatement.setLong(index++, racunId);
                preparedStatement.setLong(index++, racunItem.getLek().getId());
                preparedStatement.setInt(index++, racunItem.getKolicina());
                return preparedStatement;
            }
        };

        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        boolean uspeh = jdbcTemplate.update(statementCreator, keyHolder) == 1;

        return uspeh ? racunItem : null;
    }

    @Transactional
    public RacunItem update(RacunItem racunItem) {
        String sql = "UPDATE racun_item SET lek_id = ?, kolicina = ? WHERE id = ?";
        boolean uspeh = jdbcTemplate.update(sql, racunItem.getLek().getId(), racunItem.getKolicina(), racunItem.getId()) == 1;
        return uspeh ? racunItem : null;
    }

    @Transactional
    public RacunItem delete(Long id) {
        String sql = "DELETE FROM racun_item WHERE id = ?";

        // Execute the SQL statement and check if the deletion was successful
        int rowsAffected = jdbcTemplate.update(sql, id);
        // Return null as per your existing implementation
        return null;
    }
}
