package com.ftn.PrviMavenVebProjekat.service;

import java.util.ArrayList;
import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Lek;

public interface LekService {
	Lek findOne (Long id);
	ArrayList<Lek> findAll();
	Lek save(Lek lek);
	Lek update(Lek lek);
	Lek delete(Long id);
	List<Lek> searchLekovi(String searchTerm);
}
