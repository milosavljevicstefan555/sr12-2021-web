package com.ftn.PrviMavenVebProjekat.service;

import java.util.ArrayList;
import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;

public interface LoyaltyKarticaService {

	LoyaltyKartica findOne (Korisnik korisnik);
	ArrayList<LoyaltyKartica> findAll();
	LoyaltyKartica save (LoyaltyKartica lKartica);
	LoyaltyKartica update (LoyaltyKartica lKartica);
	LoyaltyKartica delete (Korisnik korisnik);
	void approveLoyaltyCard(String cardId);
	void denyLoyaltyCard(String cardId);
	List<LoyaltyKartica> findAllNeodobrene();
	
}
