package com.ftn.PrviMavenVebProjekat.service;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.bean.StatusLoyaltyEnum;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.NabavkaLekova;

public interface NabavkaLekovaService {

    NabavkaLekova createNabavkaLekova(NabavkaLekova nabavkaLekova);

    NabavkaLekova getNabavkaLekovaById(long id);

    List<NabavkaLekova> getAllNabavkaLekova();

    List<NabavkaLekova> getNabavkaLekovaByApotekar(Korisnik apotekar);

    List<NabavkaLekova> getNabavkaLekovaByStatus(StatusLoyaltyEnum status);

    NabavkaLekova approveNabavkaLekova(long id, String razlogOdobrenja);

    NabavkaLekova rejectNabavkaLekova(long id, String razlogOdbijanja);

    NabavkaLekova updateNabavkaLekova(long id, NabavkaLekova nabavkaLekova);

    void deleteNabavkaLekova(long id);
}
