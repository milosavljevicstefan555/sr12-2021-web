package com.ftn.PrviMavenVebProjekat.service.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;

import com.ftn.PrviMavenVebProjekat.bean.StatusLoyaltyEnum;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.model.NabavkaLekova;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.LekService;
import com.ftn.PrviMavenVebProjekat.service.NabavkaLekovaService;

@Repository
public class NabavkaLekovaServiceImpl implements NabavkaLekovaService{

	@Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private KorisnikService korisnikService;
    @Autowired
    private LekService lekService;
	
	private class NabavkaRowCallbackHandler implements RowCallbackHandler {
		private List<NabavkaLekova> nabavke = new ArrayList<NabavkaLekova>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			long id = rs.getLong(index++);
			Korisnik apotekar = korisnikService.findOne(rs.getString(index++));
			Timestamp timestamp = rs.getTimestamp(index++);
	        LocalDateTime datumIVreme = timestamp.toLocalDateTime();
	        StatusLoyaltyEnum status = StatusLoyaltyEnum.valueOf(rs.getString(index++));
	        String razlogNabavke = rs.getString(index++);
	        String razlogOdbijanja = rs.getString(index++);
	        Lek lek = lekService.findOne(rs.getLong(index++));
	        int kolicinaNabavke = rs.getInt(index++);
	        
	        NabavkaLekova nabavka = new NabavkaLekova(id, apotekar, datumIVreme, status, razlogNabavke, razlogOdbijanja, lek, kolicinaNabavke);
	        nabavke.add(nabavka);
		}

		private List<NabavkaLekova> getNabavke() {
			return nabavke;
		}
		
	}

	@Override
	public List<NabavkaLekova> getAllNabavkaLekova() {
		String sql = "SELECT * FROM nabavka_lekova";
		NabavkaRowCallbackHandler rowCallbackHandler = new NabavkaRowCallbackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.getNabavke();
	}
	@Override
	public NabavkaLekova createNabavkaLekova(NabavkaLekova nabavkaLekova) {
	    String sql = "INSERT INTO nabavka_lekova (apotekar_email, datum_i_vreme, status, razlog_nabavke, razlog_odbijanja, lek_id, kolicina_nabavke) VALUES (?, ?, ?, ?, ?, ?, ?)";
	    int rowsAffected = jdbcTemplate.update(sql, nabavkaLekova.getApotekar().getEmail(), nabavkaLekova.getDatumIVreme(), nabavkaLekova.getStatus().name(),
	            nabavkaLekova.getRazlogNabavke(), nabavkaLekova.getRazlogOdbijanja(), nabavkaLekova.getLek().getId(), nabavkaLekova.getKolicinaNabavke());

	    return rowsAffected == 1 ? nabavkaLekova : null;
	}

	@Override
	public NabavkaLekova getNabavkaLekovaById(long id) {
	    String sql = "SELECT * FROM nabavka_lekova WHERE id = ?";
	    NabavkaRowCallbackHandler rowCallbackHandler = new NabavkaRowCallbackHandler();
	    jdbcTemplate.query(sql, rowCallbackHandler, id);
	    List<NabavkaLekova> nabavke = rowCallbackHandler.getNabavke();
	    return nabavke.isEmpty() ? null : nabavke.get(0);
	}

	@Override
	public List<NabavkaLekova> getNabavkaLekovaByApotekar(Korisnik apotekar) {
	    String sql = "SELECT * FROM nabavka_lekova WHERE apotekar_email = ?";
	    NabavkaRowCallbackHandler rowCallbackHandler = new NabavkaRowCallbackHandler();
	    jdbcTemplate.query(sql, rowCallbackHandler, apotekar.getEmail());
	    return rowCallbackHandler.getNabavke();
	}

	@Override
	public List<NabavkaLekova> getNabavkaLekovaByStatus(StatusLoyaltyEnum status) {
	    String sql = "SELECT * FROM nabavka_lekova WHERE status = ?";
	    NabavkaRowCallbackHandler rowCallbackHandler = new NabavkaRowCallbackHandler();
	    jdbcTemplate.query(sql, rowCallbackHandler, status.toString());
	    return rowCallbackHandler.getNabavke();
	}

	@Override
	public NabavkaLekova approveNabavkaLekova(long id, String razlogOdobrenja) {
	    String sql = "UPDATE nabavka_lekova SET status = ?, razlog_odbijanja = ? WHERE id = ?";
	    int rowsAffected = jdbcTemplate.update(sql, StatusLoyaltyEnum.APPROVED.toString(), razlogOdobrenja, id);

	    if (rowsAffected == 1) {
	        return getNabavkaLekovaById(id);
	    } else {
	        return null;
	    }
	}

	@Override
	public NabavkaLekova rejectNabavkaLekova(long id, String razlogOdbijanja) {
	    String sql = "UPDATE nabavka_lekova SET status = ?, razlog_odbijanja = ? WHERE id = ?";
	    int rowsAffected = jdbcTemplate.update(sql, StatusLoyaltyEnum.DENIED.toString(), razlogOdbijanja, id);

	    if (rowsAffected == 1) {
	        return getNabavkaLekovaById(id);
	    } else {
	        return null;
	    }
	}

	@Override
	public NabavkaLekova updateNabavkaLekova(long id, NabavkaLekova nabavkaLekova) {
	    String sql = "UPDATE nabavka_lekova SET apotekar_email = ?, datum_ivreme = ?, status = ?, razlog_nabavke = ?, razlog_odbijanja = ?, lek_id = ?, kolicina_nabavke = ? WHERE id = ?";
	    int rowsAffected = jdbcTemplate.update(sql, nabavkaLekova.getApotekar().getEmail(), nabavkaLekova.getDatumIVreme(),
	            nabavkaLekova.getStatus().name(), nabavkaLekova.getRazlogNabavke(), nabavkaLekova.getRazlogOdbijanja(),
	            nabavkaLekova.getLek().getId(), nabavkaLekova.getKolicinaNabavke(), id);

	    if (rowsAffected == 1) {
	        return getNabavkaLekovaById(id);
	    } else {
	        return null;
	    }
	}

	@Override
	public void deleteNabavkaLekova(long id) {
	    String sql = "DELETE FROM nabavka_lekova WHERE id = ?";
	    jdbcTemplate.update(sql, id);
	}


}
