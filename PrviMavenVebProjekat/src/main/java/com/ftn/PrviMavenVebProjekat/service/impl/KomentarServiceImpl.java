package com.ftn.PrviMavenVebProjekat.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.KomentarDAO;
import com.ftn.PrviMavenVebProjekat.model.Komentar;
import com.ftn.PrviMavenVebProjekat.service.KomentarService;

@Service
public class KomentarServiceImpl implements KomentarService {
	
	@Autowired
	private KomentarDAO komentarDAO;

	@Override
	public Komentar findOne(Long id) {
		return komentarDAO.findOne(id);
	}

	@Override
	public ArrayList<Komentar> findAll() {
		return komentarDAO.findAll();
	}

	@Override
	public Komentar save(Komentar komentar) {
		return komentarDAO.save(komentar);
	}

	@Override
	public Komentar update(Komentar komentar) {
		return komentarDAO.update(komentar);
	}

	@Override
	public Komentar delete(Long id) {
		return komentarDAO.delete(id);
	}

	@Override
	public double calculateAverageRatingForLek(Long lekId) {
		return komentarDAO.calculateAverageRatingForLek(lekId);
	}

}
