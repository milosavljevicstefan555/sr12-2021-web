package com.ftn.PrviMavenVebProjekat.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.ProizvodjacLekaDAO;
import com.ftn.PrviMavenVebProjekat.model.ProizvodjacLeka;
import com.ftn.PrviMavenVebProjekat.service.ProizvodjacLekaService;

@Service
public class ProizvodjacLekaServiceImpl implements ProizvodjacLekaService{

	@Autowired
	private ProizvodjacLekaDAO proizvodjacLekaDAO;
	
	@Override
	public ProizvodjacLeka findOne(Long id) {
		return proizvodjacLekaDAO.findOne(id);
	}

	@Override
	public ArrayList<ProizvodjacLeka> findAll() {
		return proizvodjacLekaDAO.findAll();
	}

	@Override
	public ProizvodjacLeka save(ProizvodjacLeka pLeka) {
		return proizvodjacLekaDAO.save(pLeka);
	}

	@Override
	public ProizvodjacLeka update(ProizvodjacLeka pLeka) {
		return proizvodjacLekaDAO.update(pLeka);
	}

	@Override
	public ProizvodjacLeka delete(Long id) {
		return proizvodjacLekaDAO.delete(id);
	}

}
