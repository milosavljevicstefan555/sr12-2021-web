package com.ftn.PrviMavenVebProjekat.service;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Racun;
import com.ftn.PrviMavenVebProjekat.model.RacunItem;

public interface RacunService {
    Racun findOne(Long id);
    List<Racun> findAll();
    Racun save(Racun racun);
    Racun update(Racun racun);
    void delete(Long id);
	List<Racun> findPurchaseHistoryForUser(Korisnik korisnik);
}
