package com.ftn.PrviMavenVebProjekat.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.KorisnikDAO;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;



@Service
public class KorisnikServiceImpl implements KorisnikService{

	@Autowired
	private KorisnikDAO korisnikDAO;
	
	@Override
	public Korisnik findOneById(String korisnickoIme) {
		return korisnikDAO.findOne(korisnickoIme);
	}

	@Override
	public Korisnik findOne(String email) {
		return korisnikDAO.findOne(email);
	}

	@Override
	public Korisnik findOne(String email, String sifra) {
		return korisnikDAO.findOne(email, sifra);
	}

	@Override
	public List<Korisnik> findAll() {
		return korisnikDAO.findAll();
	}

	@Override
	public Korisnik save(Korisnik korisnik) {
		korisnikDAO.save(korisnik);
		return korisnik;
	}

	@Override
	public Korisnik update(Korisnik korisnik) {
		korisnikDAO.update(korisnik);
		return korisnik;
	}

	@Override
	public Korisnik delete(String korisnickoIme) {
		Korisnik korisnik = korisnikDAO.findOne(korisnickoIme);
		korisnikDAO.delete(korisnickoIme);
		return korisnik;
	}

	@Override
	public List<Korisnik> findAllNeodobrene() {
		return korisnikDAO.findAllNeodobrene();
	}

	@Override
	public void approveUser(Korisnik korisnik) {
		korisnikDAO.approveUser(korisnik);
	}

	@Override
	public void denyUser(Korisnik korisnik) {
		korisnikDAO.denyUser(korisnik);
		
	}

}
