package com.ftn.PrviMavenVebProjekat.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.model.KorpaItem;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.service.KorpaItemService;
import com.ftn.PrviMavenVebProjekat.service.LekService;

@Repository
public class KorpaItemServiceImpl implements KorpaItemService {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private LekService lekService;
    
    private class KorpaItemRowCallbackHandler implements RowCallbackHandler {
        
        private List<KorpaItem> korpaItems = new ArrayList<>();
        
        @Override
        public void processRow(ResultSet resultSet) throws SQLException {
            int index = 1;
            Long id = resultSet.getLong(index++);
            Lek lek = lekService.findOne(resultSet.getLong(index++));
            int kolicina = resultSet.getInt(index++);
            
            KorpaItem korpaItem = new KorpaItem(id, lek, kolicina);
            
            korpaItems.add(korpaItem);
        }
        
        private List<KorpaItem> getKorpaItems() {
            return korpaItems;
        }
    }
    
    public KorpaItem findOne(Long id) {
        String sql = "SELECT id, lek_id, kolicina FROM korpa_item WHERE id = ?";
        KorpaItemRowCallbackHandler rowCallbackHandler = new KorpaItemRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler, id);
        List<KorpaItem> korpaItems = rowCallbackHandler.getKorpaItems();
        return korpaItems.isEmpty() ? null : korpaItems.get(0);
    }
    
    public List<KorpaItem> findAll() {
        String sql = "SELECT id, lek_id, kolicina FROM apoteka.korpa_item;";
        KorpaItemRowCallbackHandler rowCallbackHandler = new KorpaItemRowCallbackHandler();
        jdbcTemplate.query(sql, rowCallbackHandler);
        return rowCallbackHandler.getKorpaItems();
    }
    
    @Transactional
    public KorpaItem save(KorpaItem korpaItem, Long korpa_id) {
    	PreparedStatementCreator statementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO korpa_item(korpa_id, lek_id, kolicina ) VALUES (?, ?, ?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setLong(index++, korpa_id);
				preparedStatement.setLong(index++, korpaItem.getLek().getId());
				preparedStatement.setInt(index++, korpaItem.getKolicina());
				return preparedStatement;
			}
		};
        
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(statementCreator, keyHolder) == 1;
        
        return uspeh?korpaItem:null;
    }
    
    @Transactional
    public KorpaItem update(KorpaItem korpaItem) {
        String sql = "UPDATE korpa_item SET lek_id = ?, kolicina = ? WHERE id = ?";
        boolean uspeh = jdbcTemplate.update(sql, korpaItem.getLek().getId(), korpaItem.getKolicina(), korpaItem.getId()) == 1;
        return uspeh?korpaItem:null;
    }
    
    @Transactional
    public KorpaItem delete(Long id) {
        String sql = "DELETE FROM korpa_item WHERE id = ?";
        
        // Execute the SQL statement and check if the deletion was successful
        int rowsAffected = jdbcTemplate.update(sql, id);
        //stavio da se vraca null
        return null;
    }
}

