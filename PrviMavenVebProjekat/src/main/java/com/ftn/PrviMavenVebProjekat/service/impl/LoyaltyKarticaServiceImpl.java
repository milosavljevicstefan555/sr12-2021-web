package com.ftn.PrviMavenVebProjekat.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.LoyaltyKarticaDAO;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;
import com.ftn.PrviMavenVebProjekat.service.LoyaltyKarticaService;

@Service
public class LoyaltyKarticaServiceImpl implements LoyaltyKarticaService {

	@Autowired
	private LoyaltyKarticaDAO loyaltyKarticaDAO;
	
	@Override
	public LoyaltyKartica findOne(Korisnik korisnik) {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.findOne(korisnik);
	}

	@Override
	public ArrayList<LoyaltyKartica> findAll() {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.findAll();
	}

	@Override
	public LoyaltyKartica save(LoyaltyKartica lKartica) {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.save(lKartica);
	}

	@Override
	public LoyaltyKartica update(LoyaltyKartica lKartica) {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.update(lKartica);
	}

	@Override
	public LoyaltyKartica delete(Korisnik korisnik) {
		// TODO Auto-generated method stub
		return loyaltyKarticaDAO.delete(korisnik);
	}

	@Override
	public void approveLoyaltyCard(String cardId) {
		loyaltyKarticaDAO.approveLoyaltyCard(cardId);
	}

	@Override
	public void denyLoyaltyCard(String cardId) {
		loyaltyKarticaDAO.denyLoyaltyCard(cardId);
		
	}

	@Override
	public List<LoyaltyKartica> findAllNeodobrene() {
		return loyaltyKarticaDAO.findAllNeodobrene();
	}

}
