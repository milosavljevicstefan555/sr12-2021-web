package com.ftn.PrviMavenVebProjekat.service;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.RacunItem;


public interface RacunItemService {
	RacunItem findOne(Long id);
    List<RacunItem> findAll();
    RacunItem save(RacunItem racunItem, Long korpa_id);
    RacunItem update(RacunItem racunItem);
    RacunItem delete(Long id);
}
