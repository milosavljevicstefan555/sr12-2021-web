package com.ftn.PrviMavenVebProjekat.dao;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;

public interface KorisnikDAO {
	
	public Korisnik findOne(String korisnickoIme); 
	
	public Korisnik findOne(String korisnickoIme, String lozinka);

	public List<Korisnik> findAll();

	public int save(Korisnik korisnik);

	public int update(Korisnik korisnik);

	public int delete(String korisnickoIme);

	List<Korisnik> findAllNeodobrene();

	public Object approveUser(Korisnik korisnik);

	public Object denyUser(Korisnik korisnik);
	
}
