package com.ftn.PrviMavenVebProjekat.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.dao.KategorijaLekaDAO;
import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;


@Repository
public class KategorijaLekaDAOImpl implements KategorijaLekaDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KategorijaLekaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, KategorijaLeka> kategorijeLekova = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String naziv = resultSet.getString(index++);
			String namena = resultSet.getString(index++);
			String opis = resultSet.getString(index++);

			KategorijaLeka kLeka = kategorijeLekova.get(id);
			if (kLeka == null) {
				kLeka = new KategorijaLeka(id, naziv, namena, opis);
				kategorijeLekova.put(kLeka.getId(), kLeka); // dodavanje u kolekciju
			}
		}	
		
		private List<KategorijaLeka> getKategorijeLekova() {
			return new ArrayList<>(kategorijeLekova.values());
		}
	}
	
	
	
	public KategorijaLeka findOne(Long id) {
		String sql = "SELECT * FROM kategorija_leka kl WHERE kl.id = ? ORDER BY kl.id";
		KategorijaLekaRowCallBackHandler rowCallbackHandler = new KategorijaLekaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);
		return rowCallbackHandler.getKategorijeLekova().get(0);
	}

	

	@Override
	public ArrayList<KategorijaLeka> findAll() {
		String sql = "select * from kategorija_leka kl order by kl.id";
		KategorijaLekaRowCallBackHandler rowCallbackHandler = new KategorijaLekaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return (ArrayList<KategorijaLeka>) rowCallbackHandler.getKategorijeLekova();
	}

	@Transactional
	@Override
	public KategorijaLeka save(KategorijaLeka kLeka) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "INSERT INTO kategorija_leka(naziv,namena,opis) values (?,?,?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setString(index++, kLeka.getNaziv());
				preparedStatement.setString(index++, kLeka.getNamena());
				preparedStatement.setString(index++, kLeka.getOpis());	
			
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?kLeka:null;
	}

	@Transactional
	@Override
	public KategorijaLeka update(KategorijaLeka kLeka) {
		String sql = "UPDATE kategorija_leka SET naziv=?, namena=?, opis=? where id = ?";
		boolean uspeh = jdbcTemplate.update(sql,kLeka.getNaziv(), kLeka.getNamena(), kLeka.getOpis(), kLeka.getId()) == 1;
		return uspeh?kLeka:null;
	}

	@Transactional
	@Override
	public KategorijaLeka delete(Long id) {
		String sql = "DELETE FROM kategorija_leka where id = ?";
		jdbcTemplate.update(sql, id);
		return null;
	}

}
