package com.ftn.PrviMavenVebProjekat.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.bean.UlogaEnum;
import com.ftn.PrviMavenVebProjekat.dao.KorisnikDAO;

@Repository
public class KorisnikDAOImpl implements KorisnikDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class KorisnikRowCallBackHandler implements RowCallbackHandler {

		private Map<String, Korisnik> korisnici = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			String korisnickoIme = resultSet.getString(index++);
			String lozinka = resultSet.getString(index++);
			String email = resultSet.getString(index++);
			String ime = resultSet.getString(index++);
			String prezime = resultSet.getString(index++);
			Date datumRodjenja = resultSet.getDate(index++);
			String adresa = resultSet.getString(index++);
			String brojTelefona = resultSet.getString(index++);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime vremeRegistracije = LocalDateTime.parse(resultSet.getString(index++), formatter);
			UlogaEnum ulogaKorisnika = UlogaEnum.valueOf(resultSet.getString(index++));
			boolean aktivan = resultSet.getBoolean(index++);

			Korisnik korisnik = korisnici.get(korisnickoIme);
			if (korisnik == null) {
				korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumRodjenja, adresa, brojTelefona, vremeRegistracije, ulogaKorisnika, aktivan);
				korisnici.put(korisnik.getKorisnickoIme(), korisnik); // dodavanje u kolekciju
			}
		}

		public List<Korisnik> getKorisnici() {
			return new ArrayList<>(korisnici.values());
		}

	}
	
	public Korisnik findOne(String email) {
		String sql = 
				"SELECT * FROM korisnik kor " + 
				"WHERE kor.email = ? ";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, email);

		return rowCallbackHandler.getKorisnici().get(0);
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		String sql = 
				"SELECT * FROM korisnik kor " + 
				"WHERE kor.korisnicko_ime = ? AND " +
				"kor.lozinka = ? AND kor.aktivan = true " + 
				"ORDER BY kor.korisnicko_ime";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, korisnickoIme, lozinka);

		if(rowCallbackHandler.getKorisnici().size() == 0) {
			return null;
		}
		
		return rowCallbackHandler.getKorisnici().get(0);
	}
	@Override
	public List<Korisnik> findAll() {
		String sql = 
				"SELECT * FROM korisnik kor WHERE kor.aktivan = true " + 
				"ORDER BY kor.email";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKorisnici();
	}
	
	@Override
	public List<Korisnik> findAllNeodobrene() {
		String sql = 
				"SELECT * FROM korisnik kor WHERE kor.aktivan = false " + 
				"ORDER BY kor.email";

		KorisnikRowCallBackHandler rowCallbackHandler = new KorisnikRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKorisnici();
	}
	
	@Transactional
	@Override
	public int save(Korisnik korisnik) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "insert into korisnik(korisnicko_ime, lozinka, email, ime, prezime, datum_rodjenja, adresa, broj_telefona, datum_registracije, uloga, aktivan)"
						+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setString(index++, korisnik.getKorisnickoIme());
				preparedStatement.setString(index++, korisnik.getLoznka());
				preparedStatement.setString(index++, korisnik.getEmail());
				preparedStatement.setString(index++, korisnik.getIme());
				preparedStatement.setString(index++, korisnik.getPrezime());
				//proveriti
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String formattedDate = dateFormat.format(korisnik.getDatumRodjenja());
				preparedStatement.setString(index++, formattedDate);
				preparedStatement.setString(index++, korisnik.getAdresa());
				preparedStatement.setInt(index++, Integer.valueOf(korisnik.getBrojTelefona()));
				//ovde isto
				DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				String formattedDateTime = korisnik.getVremeRegistracije().format(dateTimeFormat);
				preparedStatement.setString(index++, formattedDateTime);
				preparedStatement.setString(index++, korisnik.getUlogaKorisnika().toString());
				preparedStatement.setBoolean(index++, korisnik.isAktivan());
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?1:null;
		
	}
	
	@Transactional
	@Override
	public int update(Korisnik korisnik) {
		String sql = "UPDATE korisnik SET  lozinka = ?, ime = ?, prezime = ?, datum_rodjenja = ?, adresa = ?, broj_telefona = ?, datum_registracije = ?, uloga = ?, aktivan = ?  WHERE korisnicko_ime = ?";	
		boolean uspeh = jdbcTemplate.update(sql, korisnik.getLoznka(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getDatumRodjenja(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.getVremeRegistracije(), korisnik.getUlogaKorisnika(), korisnik.isAktivan(), korisnik.getKorisnickoIme()) == 1;
		return uspeh?1:0;
	}
	
	@Transactional
	@Override
	public int delete(String korisnickoIme) {
		String sql = "DELETE FROM korisnici WHERE korisnicko_ime = ?";
		return jdbcTemplate.update(sql, korisnickoIme);
	}

	@Override
	public Object approveUser(Korisnik korisnik) {
		String sql = "UPDATE korisnik Set aktivan = ?  WHERE korisnicko_ime = ?";	
		boolean uspeh = jdbcTemplate.update(sql, true, korisnik.getKorisnickoIme()) == 1;
		return uspeh?1:0;
	}

	@Override
	public Object denyUser(Korisnik korisnik) {
		String sql = "DELETE FROM korisnici WHERE korisnicko_ime = ?";
		jdbcTemplate.update(sql, korisnik.getKorisnickoIme());
		System.out.println("korisnik obrisan");
		return null;
	}


}