package com.ftn.PrviMavenVebProjekat.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.dao.KomentarDAO;
import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;
import com.ftn.PrviMavenVebProjekat.model.Komentar;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.LekService;

@Repository
public class KomentarDAOImpl implements KomentarDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private LekService lekService;
	
	private class KomentarRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Komentar> komentari = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String tekstKomentara = rs.getString(index++);
			int ocena = rs.getInt(index++);
			String datumObjavljivanjaString = rs.getString(index++);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = null;
			try {
				date = sdf.parse(datumObjavljivanjaString);
			} catch (ParseException e) {
				System.out.println("nije uspelo pretvaranje datuma iz baze");
				e.printStackTrace();
			}
			Korisnik korisnik = korisnikService.findOne(rs.getString(index++));
			Lek lek = lekService.findOne(rs.getLong(index++));
			boolean anoniman = rs.getBoolean(index++);
			
			Komentar komentar = komentari.get(id);
			if (komentar == null) {
				komentar = new Komentar(id, tekstKomentara, ocena, date, korisnik, lek, anoniman);
				komentari.put(id, komentar);
			}
		}
		
		private List<Komentar> getKomentari() {
			return new ArrayList<>(komentari.values());
		}
	}

	@Override
	public Komentar findOne(Long id) {
		String sql = "select * from komentar k where k.id = ? order by k.id";
		KomentarRowCallBackHandler rowCallBackHandler = new KomentarRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		return rowCallBackHandler.getKomentari().get(0);
	}

	@Override
	public ArrayList<Komentar> findAll() {
		String sql = "select * from komentar k order by k.id";
		KomentarRowCallBackHandler rowCallBackHandler = new KomentarRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return (ArrayList<Komentar>) rowCallBackHandler.getKomentari();
	}

	@Transactional
	@Override
	public Komentar save(Komentar komentar) {
		PreparedStatementCreator statementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "insert into komentar(tekst_komentara, ocena, datum_objavljivanja, autor_email, lek_id, anoniman) values (?, ?, ?, ?, ?, ?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, komentar.getTekstKomentara());
				preparedStatement.setInt(index++, komentar.getOcena());
				preparedStatement.setString(index++, komentar.getDatumObjavljivanja().getYear() + "-" + komentar.getDatumObjavljivanja().getMonth() + "-" + komentar.getDatumObjavljivanja().getDate());
				preparedStatement.setString(index++, komentar.getAutor().getEmail());
				preparedStatement.setLong(index++, komentar.getLek().getId());
				preparedStatement.setBoolean(index++, komentar.isAnoniman());
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(statementCreator, keyHolder) == 1;
		return uspeh?komentar:null;
	}

	@Transactional
	@Override
	public Komentar update(Komentar komentar) {
		String sql = "update komentar set tekst_komentara = ?, ocena = ?,datum_objavljivanja = ?, autor_email = ?, lek_id = ?, anoniman = ? where id = ?";
		boolean uspeh = jdbcTemplate.update(sql,  komentar.getTekstKomentara(), komentar.getOcena(), komentar.getDatumObjavljivanja().getYear() + "-" + komentar.getDatumObjavljivanja().getMonth() + "-" + komentar.getDatumObjavljivanja().getDate(), komentar.getAutor().getEmail(), komentar.getLek().getId(), komentar.isAnoniman(), komentar.getId()) == 1;
		return uspeh?komentar:null;
	}

	@Transactional
	@Override
	public Komentar delete(Long id) {
		String sql = "delete from komentar where id = ?";
		jdbcTemplate.update(sql, id);
		return null;
	}

	@Override
	public double calculateAverageRatingForLek(Long lekId) {
	    String sql = "SELECT AVG(ocena) FROM komentar WHERE lek_id = ?";
	    Double averageRating = jdbcTemplate.queryForObject(sql, Double.class, lekId);
	    return averageRating != null ? averageRating : 0.0;
	}


}
