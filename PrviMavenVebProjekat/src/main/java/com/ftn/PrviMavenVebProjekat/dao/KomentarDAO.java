package com.ftn.PrviMavenVebProjekat.dao;

import java.util.ArrayList;

import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;
import com.ftn.PrviMavenVebProjekat.model.Komentar;

public interface KomentarDAO {
	Komentar findOne(Long id);
	ArrayList<Komentar> findAll();
	Komentar save(Komentar komentar);
	Komentar update(Komentar komentar);
	Komentar delete(Long id);
	double calculateAverageRatingForLek(Long lekId);
}
