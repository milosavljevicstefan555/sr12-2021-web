package com.ftn.PrviMavenVebProjekat.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.bean.OblikEnum;
import com.ftn.PrviMavenVebProjekat.dao.LekDAO;
import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.model.ProizvodjacLeka;
import com.ftn.PrviMavenVebProjekat.service.KategorijaLekaService;
import com.ftn.PrviMavenVebProjekat.service.ProizvodjacLekaService;

@Repository
public class LekDAOImpl implements LekDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ProizvodjacLekaService proizvodjacLekaService;
	@Autowired
	private KategorijaLekaService kategorijaLekaService;
	
	private class LekRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Lek> lekovi = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String naziv = resultSet.getString(index++);
//			String namena = resultSet.getString(index++);
			String opis = resultSet.getString(index++);
			String kontraindikacije = resultSet.getString(index++);
			OblikEnum oblik = OblikEnum.valueOf(resultSet.getString(index++));
			double prosecnaOcena = resultSet.getDouble(index++);
			String slikaURL = resultSet.getString(index++);
			int dostupnaKolicina = resultSet.getInt(index++);
			double cena = resultSet.getDouble(index++);
			ProizvodjacLeka proizvodjac = proizvodjacLekaService.findOne(resultSet.getLong(index++));
			KategorijaLeka kategorija = kategorijaLekaService.findOne(resultSet.getLong(index++));
			
			Lek lek = lekovi.get(id);
			if (lek == null) {
				lek = new Lek(id, naziv, opis, kontraindikacije, oblik, prosecnaOcena, slikaURL, dostupnaKolicina, cena, proizvodjac, kategorija);
				
				lekovi.put(lek.getId(), lek); // dodavanje u kolekciju
			}
		}	
		
		private List<Lek> getLekovi() {
			return new ArrayList<>(lekovi.values());
		}
	}
	
	
	@Override
	public Lek findOne(Long id) {
		String sql = "select * from lek where lek.id = ? order by lek.id";
		LekRowCallBackHandler rowCallBackHandler = new LekRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		return rowCallBackHandler.getLekovi().get(0);
	}

	@Override
	public ArrayList<Lek> findAll() {
		String sql = "select * from lek order by lek.id";
		LekRowCallBackHandler rowCallBackHandler = new LekRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return (ArrayList<Lek>) rowCallBackHandler.getLekovi();
	}
	
	@Override
	public List<Lek> searchLekovi(String searchTerm) {
	    String sql = "SELECT * FROM lek WHERE naziv LIKE '%" + searchTerm + "%'";
	    LekRowCallBackHandler rowCallBackHandler = new LekRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);

		return (ArrayList<Lek>) rowCallBackHandler.getLekovi();
	}

	@Transactional
	@Override
	public Lek save(Lek lek) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "insert into lek(naziv, opis, kontraindikacije, oblik, prosecna_ocena, slika, dostupna_kolicina, cena, proizvodjac_id, kategorija_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setString(index++, lek.getNaziv());
				preparedStatement.setString(index++, lek.getOpis());
				preparedStatement.setString(index++, lek.getKontraindikacije());
				preparedStatement.setString(index++, lek.getOblik().toString());
				preparedStatement.setDouble(index++, lek.getProsecnaOcena()); 
				preparedStatement.setString(index++, lek.getSlikaURL());
				preparedStatement.setInt(index++, lek.getDostupnaKolicina());
				preparedStatement.setDouble(index++, lek.getCena());
				preparedStatement.setLong(index++, lek.getProizvodjac().getId());
				preparedStatement.setLong(index++, lek.getKategorija().getId());
				
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?lek:null;
	}

	@Transactional
	@Override
	public Lek update(Lek lek) {
		String sql = "update lek set naziv = ?,opis = ?, kontraindikacije = ?, oblik = ?, prosecna_ocena = ?, slika = ?, dostupna_kolicina = ?, cena = ?, proizvodjac_id = ?, kategorija_id = ? where id = ?";
		boolean uspeh = jdbcTemplate.update(sql, lek.getNaziv(), lek.getOpis(), lek.getKontraindikacije(), lek.getOblik().toString(), lek.getProsecnaOcena(), lek.getSlikaURL(), lek.getDostupnaKolicina(), lek.getCena(), lek.getProizvodjac().getId(), lek.getKategorija().getId(), lek.getId()) == 1;
		return uspeh?lek:null;
	}

	@Transactional
	@Override
	public Lek delete(Long id) {
		String sql = "delete from lek where id = ?";
		jdbcTemplate.update(sql, id);
		return null;
	}

}
