package com.ftn.PrviMavenVebProjekat.dao;

import java.util.ArrayList;
import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;

public interface LoyaltyKarticaDAO {
	LoyaltyKartica findOne (Korisnik korisnik);
	ArrayList<LoyaltyKartica> findAll();
	LoyaltyKartica save (LoyaltyKartica lKartica);
	LoyaltyKartica update (LoyaltyKartica lKartica);
	LoyaltyKartica delete (Korisnik korisnik);
	Object approveLoyaltyCard(String cardId);
	void denyLoyaltyCard(String cardId);
	List<LoyaltyKartica> findAllNeodobrene();
}
