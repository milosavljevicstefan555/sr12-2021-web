package com.ftn.PrviMavenVebProjekat.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.dao.ProizvodjacLekaDAO;
import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;
import com.ftn.PrviMavenVebProjekat.model.ProizvodjacLeka;

@Repository
public class ProizvodjacLekaDAOImpl implements ProizvodjacLekaDAO{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class ProizvodjacLekaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, ProizvodjacLeka> proizvodjaciLekova = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			Long id = resultSet.getLong(index++);
			String naziv = resultSet.getString(index++);
			String drzavaSediste = resultSet.getString(index++);

			ProizvodjacLeka pLeka = proizvodjaciLekova.get(id);
			if (pLeka == null) {
				pLeka = new ProizvodjacLeka(id, naziv, drzavaSediste);
				proizvodjaciLekova.put(pLeka.getId(), pLeka); // dodavanje u kolekciju
			}
		}	
		
		private List<ProizvodjacLeka> getProizvodjaciLekova() {
			return new ArrayList<>(proizvodjaciLekova.values());
		}
	}

	@Override
	public ProizvodjacLeka findOne(Long id) {
		String sql = "select * from proizvodjac_leka pl where pl.id = ? order by pl.id";
		ProizvodjacLekaRowCallBackHandler rowCallBackHandler = new ProizvodjacLekaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, id);
		
		return rowCallBackHandler.getProizvodjaciLekova().get(0);
	}

	@Override
	public ArrayList<ProizvodjacLeka> findAll() {
		String sql = "select * from proizvodjac_leka pl order by pl.id";
		ProizvodjacLekaRowCallBackHandler rowCallBackHandler = new ProizvodjacLekaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler);
		return (ArrayList<ProizvodjacLeka>) rowCallBackHandler.getProizvodjaciLekova();
	}

	@Transactional
	@Override
	public ProizvodjacLeka save(ProizvodjacLeka pLeka) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "insert into proizvodjac_leka(naziv, drzava_sedista) values (?,?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				
				preparedStatement.setString(index++, pLeka.getNaziv());
				preparedStatement.setString(index++, pLeka.getDrzavaSediste());
				return preparedStatement;
			}
			
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		return uspeh?pLeka:null;
	}

	@Transactional
	@Override
	public ProizvodjacLeka update(ProizvodjacLeka pLeka) {
		String sql = "update proizvodjac_leka set naziv=?, drzava_sedista = ? where id = ?";
		boolean uspeh = jdbcTemplate.update(sql, pLeka.getNaziv(), pLeka.getDrzavaSediste(), pLeka.getId()) == 1;
		return uspeh?pLeka:null;
	}

	@Transactional
	@Override
	public ProizvodjacLeka delete(Long id) {
		String sql = "delete from proizvodjac_leka where id = ?";
		jdbcTemplate.update(sql, id);
		return null;
	}

}
