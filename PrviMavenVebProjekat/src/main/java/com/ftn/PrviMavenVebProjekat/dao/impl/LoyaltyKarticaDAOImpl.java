package com.ftn.PrviMavenVebProjekat.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.PrviMavenVebProjekat.bean.StatusLoyaltyEnum;
import com.ftn.PrviMavenVebProjekat.dao.LoyaltyKarticaDAO;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;

@Repository
public class LoyaltyKarticaDAOImpl implements LoyaltyKarticaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KorisnikService korisnikService;
	
	private class LoyaltyKarticaRowCallBackHandler implements RowCallbackHandler {

		private Map<Korisnik, LoyaltyKartica> kartice = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet rs) throws SQLException {
			int index = 1;
			String email = rs.getString(index++);
			String popust = rs.getString(index++);
			int brojPoena = rs.getInt(index++);
			StatusLoyaltyEnum status = StatusLoyaltyEnum.valueOf(rs.getString(index++));
			
			Korisnik korisnik = korisnikService.findOne(email);
			
			LoyaltyKartica kartica = kartice.get(korisnik);
			
			if (kartica == null) {
				kartica = new LoyaltyKartica(korisnik, popust, brojPoena, status);
				kartice.put(korisnik, kartica);
			}
			
		}
		
		private List<LoyaltyKartica> getLoyaltyKartice() {
			return new ArrayList<>(kartice.values());
		}
		
	}
	
	@Override
	public LoyaltyKartica findOne(Korisnik korisnik) {
		String sql = "SELECT * FROM loyalty_kartica lk WHERE lk.email_korisnika = ? AND lk.status = 'APPROVED' ORDER BY lk.email_korisnika";
		LoyaltyKarticaRowCallBackHandler rowCallBackHandler = new LoyaltyKarticaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallBackHandler, korisnik.getEmail());
		LoyaltyKartica kartica = null;
		try {
			kartica = rowCallBackHandler.getLoyaltyKartice().get(0); 
		}catch (Exception e) {
			// TODO: handle exception
		}
		return kartica;
	}

	@Override
	public ArrayList<LoyaltyKartica> findAll() {
		String sql = "SELECT * FROM loyalty_kartica lk WHERE lk.status = 'APPROVED' ORDER BY lk.email_korisnika";
		LoyaltyKarticaRowCallBackHandler backHandler = new LoyaltyKarticaRowCallBackHandler();
		jdbcTemplate.query(sql, backHandler);
		return (ArrayList<LoyaltyKartica>) backHandler.getLoyaltyKartice();
	}

	@Transactional
	@Override
	public LoyaltyKartica save(LoyaltyKartica lKartica) {
		PreparedStatementCreator statementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				String sql = "insert into loyalty_kartica(email_korisnika, popust, broj_poena, status) values (?, ?, ?, ?)";
				PreparedStatement preparedStatement = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, lKartica.getKorisnik().getEmail());
				preparedStatement.setString(index++, lKartica.getPopust());
				preparedStatement.setInt(index++, lKartica.getBrojPoena());
				preparedStatement.setString(index++, lKartica.getStatus().toString());
				return preparedStatement;
			}
		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(statementCreator, keyHolder) == 1;
		return uspeh?lKartica:null;
	}

	@Transactional
	@Override
	public LoyaltyKartica update(LoyaltyKartica lKartica) {
		String sql = "update loyalty_kartica set popust = ?, broj_poena = ?, status = ? where email_korisnika = ?";
		boolean uspeh = jdbcTemplate.update(sql, lKartica.getPopust(), lKartica.getBrojPoena(), lKartica.getStatus().toString(), lKartica.getKorisnik().getEmail()) == 1;
		return uspeh?lKartica:null;
	}

	@Transactional
	@Override
	public LoyaltyKartica delete(Korisnik korisnik) {
		String sql = "delete from loyalty_kartica where email_korisnika = ?";
		jdbcTemplate.update(sql, korisnik.getEmail());
		return null;
	}

	@Override
	public Object approveLoyaltyCard(String cardId) {
		String sql = "update loyalty_kartica set status = ? where email_korisnika = ?";
		//cardId je email mrzi me da menjam
		jdbcTemplate.update(sql,StatusLoyaltyEnum.APPROVED.toString(), cardId);
		return null;
	}

	@Override
	public void denyLoyaltyCard(String cardId) {
		String sql = "update loyalty_kartica set status = ? where email_korisnika = ?";
		//cardId je email mrzi me da menjam
		jdbcTemplate.update(sql,StatusLoyaltyEnum.DENIED, cardId);
	}

	@Override
	public List<LoyaltyKartica> findAllNeodobrene() {
		String sql = "SELECT * FROM loyalty_kartica lk WHERE lk.status = 'PENDING' ORDER BY lk.email_korisnika";
		LoyaltyKarticaRowCallBackHandler backHandler = new LoyaltyKarticaRowCallBackHandler();
		jdbcTemplate.query(sql, backHandler);
		return (ArrayList<LoyaltyKartica>) backHandler.getLoyaltyKartice();
	}

}
