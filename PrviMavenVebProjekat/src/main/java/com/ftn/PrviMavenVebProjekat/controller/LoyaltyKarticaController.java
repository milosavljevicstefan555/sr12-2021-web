package com.ftn.PrviMavenVebProjekat.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.PrviMavenVebProjekat.bean.StatusLoyaltyEnum;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.LoyaltyKarticaService;

@Controller
@RequestMapping(value = "/loyaltyKartica")
public class LoyaltyKarticaController implements ServletContextAware{
	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private LoyaltyKarticaService loyaltyKarticaService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@PostConstruct
	public void init() {
	bURL = servletContext.getContextPath()+"/";
	}

	/** pribavnjanje HTML stanice za prikaz svih entiteta, get zahtev 
	 * @throws IOException */
	// GET: loyaltyKartica
	@GetMapping
	public ModelAndView index() {	
		List<LoyaltyKartica> loyaltyKartice = loyaltyKarticaService.findAll();
		ModelAndView rezultat = new ModelAndView("loyaltyKartica");
		rezultat.addObject("loyaltyKartice", loyaltyKartice);
		return rezultat;
	}

	/** pribavnjanje HTML stanice za unos novog entiteta, get zahtev 
	 * @throws IOException */
	// GET: loyaltyKartica/add
	@GetMapping(value="/add")
	public ModelAndView create(HttpSession session, HttpServletResponse response){
		ArrayList<Korisnik> korisnici = (ArrayList<Korisnik>) korisnikService.findAll();
		ArrayList<String> statusLoyaltyEnum =  new ArrayList<>();
		statusLoyaltyEnum.add(StatusLoyaltyEnum.APPROVED.toString());
		statusLoyaltyEnum.add(StatusLoyaltyEnum.DENIED.toString());
		statusLoyaltyEnum.add(StatusLoyaltyEnum.PENDING.toString());
		ModelAndView result = new ModelAndView("dodajLoyaltyKarticu");
		result.addObject("korisnici", korisnici);
		result.addObject("statusLoyaltyEnum", statusLoyaltyEnum);
		return result;
	}

	/** obrada podataka forme za unos novog entiteta, post zahtev */
	// POST: loyaltyKartica/add
	@PostMapping(value="/add")
	public void create(@RequestParam String email_korisnika, @RequestParam String popust, 
			@RequestParam int brojPoena, @RequestParam String status, HttpServletResponse response) throws IOException {		
		Korisnik korisnik = korisnikService.findOne(email_korisnika);
		LoyaltyKartica loyaltyKarticaNova = new LoyaltyKartica(korisnik, popust, brojPoena, StatusLoyaltyEnum.valueOf(status));
		loyaltyKarticaService.save(loyaltyKarticaNova);
		response.sendRedirect(bURL + "/loyaltyKartica");
	}

	/** obrada podataka forme za izmenu postojećeg entiteta, post zahtev */
	// POST: loyaltyKartica/edit
	@PostMapping(value="/edit")
	public void edit(@RequestParam String email_korisnika, @RequestParam String popust, 
			@RequestParam int brojPoena, @RequestParam String status, HttpServletResponse response) throws IOException {	
		Korisnik korisnik = korisnikService.findOne(email_korisnika);
		LoyaltyKartica loyaltyKarticaEdited = new LoyaltyKartica(korisnik, popust, brojPoena, StatusLoyaltyEnum.valueOf(status));
		loyaltyKarticaService.update(loyaltyKarticaEdited);
		
		response.sendRedirect(bURL+"loyaltyKartica");
	}
	
	@PostMapping(value="/delete")
	public void delete(@RequestParam String email_korisnika, HttpServletResponse response) throws IOException {
		Korisnik korisnik = korisnikService.findOne(email_korisnika);
		LoyaltyKartica deleted = loyaltyKarticaService.delete(korisnik);
		response.sendRedirect(bURL+"loyaltyKartica");
	}

	/** pribavnjanje HTML stanice za prikaz određenog entiteta , get zahtev

	    @throws IOException */
	    // GET: loyaltyKartica/details?id=1
	    @GetMapping(value="/details")
	    @ResponseBody
	    public ModelAndView details(@RequestParam String email_korisnika) throws IOException {
	    	ArrayList<Korisnik> korisnici = (ArrayList<Korisnik>) korisnikService.findAll();
			Korisnik korisnik = korisnikService.findOne(email_korisnika);
		    LoyaltyKartica loyaltyKartica = loyaltyKarticaService.findOne(korisnik);
		    ArrayList<String> statusLoyaltyEnum =  new ArrayList<>();
			statusLoyaltyEnum.add(StatusLoyaltyEnum.APPROVED.toString());
			statusLoyaltyEnum.add(StatusLoyaltyEnum.DENIED.toString());
			statusLoyaltyEnum.add(StatusLoyaltyEnum.PENDING.toString());
		    ModelAndView rezultat = new ModelAndView("loyaltyKarticaDetails");
		    rezultat.addObject("korisnici", korisnici);
		    rezultat.addObject("loyaltykartica", loyaltyKartica);

			rezultat.addObject("statusLoyaltyEnum", statusLoyaltyEnum);
		    return rezultat;
	    }
	 
	    @PostMapping(value = "/request")
	    public void requestLoyaltyCard(@RequestParam String korisnikEmail, HttpServletResponse response) throws IOException {
	        Korisnik korisnik = korisnikService.findOne(korisnikEmail);
	        LoyaltyKartica loyaltyKartica = new LoyaltyKartica(korisnik, "popoust", 10, StatusLoyaltyEnum.PENDING);
	        loyaltyKarticaService.save(loyaltyKartica);
	        response.sendRedirect(bURL + "/loyaltyKartica");
	    }


	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}
}
