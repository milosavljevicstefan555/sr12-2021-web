package com.ftn.PrviMavenVebProjekat.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.PrviMavenVebProjekat.bean.StatusLoyaltyEnum;
import com.ftn.PrviMavenVebProjekat.bean.UlogaEnum;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;
import com.ftn.PrviMavenVebProjekat.model.NabavkaLekova;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.LekService;
import com.ftn.PrviMavenVebProjekat.service.LoyaltyKarticaService;
import com.ftn.PrviMavenVebProjekat.service.NabavkaLekovaService;

@Controller
@RequestMapping(value = "/korisnici")
public class KorisnikController implements ServletContextAware {
	
	public static final String KORISNIK_KEY = "korisnik";
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 
	
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private LoyaltyKarticaService loyaltyKarticaService;
	@Autowired
	private NabavkaLekovaService nabavkaLekovaService;
	@Autowired
	private LekService lekService;
	
	
	/** inicijalizacija podataka za kontroler */
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 
	@GetMapping("/zahtevi")
	public ModelAndView getZahteviPage() {
	    ModelAndView modelAndView = new ModelAndView("zahtevi");
	    List<Korisnik> korisnici = korisnikService.findAllNeodobrene();
	    List<LoyaltyKartica> kartice = loyaltyKarticaService.findAllNeodobrene();
	    List<NabavkaLekova> nabavke = nabavkaLekovaService.getNabavkaLekovaByStatus(StatusLoyaltyEnum.PENDING);
	    if (korisnici == null) {
	    	korisnici = new ArrayList<Korisnik>();
	    }
	    if (kartice == null) {
	    	kartice = new ArrayList<LoyaltyKartica>();
	    }
	    
	    modelAndView.addObject("kartice", kartice);
	    modelAndView.addObject("korisnici", korisnici);
	    modelAndView.addObject("nabavke", nabavke);
	    
	    return modelAndView;
	}
	
	@PostMapping("/approveNabavka")
    @ResponseBody
    public ResponseEntity<String> approveNabavka(@RequestParam("nabavkaId") long nabavkaId) {
        NabavkaLekova nabavka = nabavkaLekovaService.getNabavkaLekovaById(nabavkaId);
        nabavka.getLek().setDostupnaKolicina(nabavka.getLek().getDostupnaKolicina() + nabavka.getKolicinaNabavke());
        lekService.update(nabavka.getLek());

        // Perform the approval logic here
        // Example:
        nabavkaLekovaService.approveNabavkaLekova(nabavkaId, "Approved");

        return ResponseEntity.ok("Nabavka approved successfully.");
    }

    @PostMapping("/denyNabavka")
    @ResponseBody
    public ResponseEntity<String> denyNabavka(@RequestParam("nabavkaId") long nabavkaId) {
        NabavkaLekova nabavka = nabavkaLekovaService.getNabavkaLekovaById(nabavkaId);
        if (nabavka == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Nabavka not found.");
        }

        // Perform the denial logic here
        // Example:
        nabavkaLekovaService.rejectNabavkaLekova(nabavkaId, "Denied");

        return ResponseEntity.ok("Nabavka denied successfully.");
    }

	@PostMapping("/approveUser")
	public void approveUser(@RequestParam String userId, HttpServletResponse response) throws IOException {
	    // Logic to approve the user with the given userId
	    // For example:
	    korisnikService.approveUser(korisnikService.findOne(userId));

	    response.sendRedirect(bURL + "zahtevi");
	}

	@PostMapping("/denyUser")
	public void denyUser(@RequestParam String userId, HttpServletResponse response) throws IOException {
	    // Logic to deny the user with the given userId
	    // For example:
	    korisnikService.denyUser(korisnikService.findOne(userId));

	    response.sendRedirect(bURL + "zahtevi");
	}
	@PostMapping("/approveLoyaltyCard")
	public void approveLoyaltyCard(@RequestParam String cardId, HttpServletResponse response) throws IOException {
	    // Logic to approve the loyalty card with the given cardId
	    // For example:
	    loyaltyKarticaService.approveLoyaltyCard(cardId);

	    response.sendRedirect(bURL + "zahtevi");
	}

	@PostMapping("/denyLoyaltyCard")
	public void denyLoyaltyCard(@RequestParam String cardId, HttpServletResponse response) throws IOException {
	    // Logic to deny the loyalty card with the given cardId
	    // For example:
	    loyaltyKarticaService.denyLoyaltyCard(cardId);

	    response.sendRedirect(bURL + "zahtevi");
	}


	@GetMapping("/addNabavkaLekova")
	public ModelAndView getAddNabavkaLekova(HttpSession session, HttpServletResponse response) {
		String korisnikKobaja = session.getAttribute("korisnik").toString();
	    String[] split = korisnikKobaja.split(";");
	    String emailUlogovanogKorisnika = split[2];
	    System.out.println(korisnikKobaja);
	    System.out.println(emailUlogovanogKorisnika);
	    ModelAndView modelAndView = new ModelAndView("nabavkaLekova");
	    modelAndView.addObject("korisnik", korisnikService.findOne(emailUlogovanogKorisnika));
	    modelAndView.addObject("lekovi", lekService.findAll());
	    return modelAndView;
	}

	
	// GET: knjige/dodaj
	@GetMapping(value="/add")
	public String create(HttpSession session, HttpServletResponse response){
			return "dodajKorisnika";
	}
	@PostMapping(value = "/addNabavkaLekova")
	public void createNabavkaLekova(@RequestParam("apotekarEmail") String apotekarEmail,
            @RequestParam("lekId") Long lekId,
            @RequestParam("kolicina") int kolicina,
            @RequestParam("razlogNabavke") String razlogNabavke,
            HttpSession session, HttpServletResponse response) throws IOException {

		Korisnik apotekarObj = korisnikService.findOne(apotekarEmail);
		Lek lekObj = lekService.findOne(lekId);

	    NabavkaLekova nabavkaLekova = new NabavkaLekova();
	    nabavkaLekova.setApotekar(apotekarObj);
	    nabavkaLekova.setLek(lekObj);
	    nabavkaLekova.setKolicinaNabavke(kolicina);
	    nabavkaLekova.setStatus(StatusLoyaltyEnum.PENDING);
	    nabavkaLekova.setRazlogNabavke(razlogNabavke);
	    nabavkaLekova.setDatumIVreme(LocalDateTime.now());

	    // Save the nabavkaLekova object
	    nabavkaLekovaService.createNabavkaLekova(nabavkaLekova);
	    response.sendRedirect(bURL + "lek");
	}


	@PostMapping(value ="/add")
	public void create(@RequestParam String korisnickoIme, @RequestParam String loznka, 
	                       @RequestParam String email, @RequestParam String ime,
	                       @RequestParam String prezime, @RequestParam String datumRodjenja,
	                       @RequestParam String adresa, @RequestParam String brojTelefona,
	                       @RequestParam UlogaEnum ulogaKorisnika, HttpServletResponse response) throws IOException {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = dateFormat.parse(datumRodjenja);
		} catch (ParseException e) {
			System.out.println("nije uspeo datum");
			e.printStackTrace();
		}
	       Korisnik noviKorisnik = new Korisnik(korisnickoIme, loznka, email, ime, prezime, 
	                                             date, adresa, brojTelefona, 
	                                            LocalDateTime.now(), ulogaKorisnika, true);
	       korisnikService.save(noviKorisnik);
	       response.sendRedirect("/korisnici");
	}
	
	@GetMapping(value = "/login")
	public void getLogin(@RequestParam(required = true) String korisnickoIme, @RequestParam(required = true) String lozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		postLogin(korisnickoIme, lozinka, session, response);
	}

	@PostMapping(value = "/login")
	@ResponseBody
	public void postLogin(@RequestParam(required = true) String korisnickoIme, @RequestParam(required = true) String lozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);
		String greska = "";
		if (korisnik == null)
			greska = "neispravni kredencijali";

		if (!greska.equals("")) {
			PrintWriter out;
			out = response.getWriter();
			File htmlFile = new File("C:/greska.html");
			Document doc = Jsoup.parse(htmlFile, "UTF-8");

			Element body = doc.select("body").first();

			if (!greska.equals("")) {
				Element divGreska = new Element(Tag.valueOf("div"), "").text(greska);
				body.appendChild(divGreska);
			}
			
			Element loginForm = new Element(Tag.valueOf("form"), "").attr("method", "post").attr("action", "korisnici/login");
			Element table = new Element(Tag.valueOf("table"), "");
			Element caption = new Element(Tag.valueOf("caption"), "").text("Prijava korisnika na sistem");
			Element trEmail = new Element(Tag.valueOf("tr"), "");
			Element thEmail = new Element(Tag.valueOf("th"), "").text("Korisnicko Ime:");
			Element tdEmail = new Element(Tag.valueOf("td"), "").appendChild(new Element(Tag.valueOf("input"), "").attr("type", "text").attr("name", "korisnickoIme"));
			Element trLozinka = new Element(Tag.valueOf("tr"), "");
			Element thLozinka = new Element(Tag.valueOf("th"), "").text("Lozinka:");
			Element tdLozinka = new Element(Tag.valueOf("td"), "").appendChild(new Element(Tag.valueOf("input"), "").attr("type", "text").attr("name", "lozinka"));
			Element trSubmit = new Element(Tag.valueOf("tr"), "");
			Element thSubmit = new Element(Tag.valueOf("th"), "");
			Element tdSubmit = new Element(Tag.valueOf("td"), "").appendChild(new Element(Tag.valueOf("input"), "").attr("type", "submit").attr("value", "Prijavi se"));
			
			trEmail.appendChild(thEmail);
			trEmail.appendChild(tdEmail);
			trLozinka.appendChild(thLozinka);
			trLozinka.appendChild(tdLozinka);
			trSubmit.appendChild(thSubmit);
			trSubmit.appendChild(tdSubmit);

			table.appendChild(caption);
			table.appendChild(trEmail);
			table.appendChild(trLozinka);
			table.appendChild(trSubmit);
			
			loginForm.appendChild(table);

			body.appendChild(loginForm);
			
			out.write(doc.html());
			return;
		}

		if (session.getAttribute(KORISNIK_KEY) != null)
			greska = "korisnik je već prijavljen na sistem morate se prethodno odjaviti<br/>";

		if (!greska.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;
			out = response.getWriter();

			StringBuilder retVal = new StringBuilder();
			retVal.append("<!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "	<meta charset=\"UTF-8\">\r\n"
					+ "	<base href=\"/PrviMavenVebProjekat/\">	\r\n" + "	<title>Prijava korisnika</title>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n"
					+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n"
					+ "</head>\r\n" + "<body>\r\n" + "	<ul>\r\n"
					+ "		<li><a href=\"registracija.html\">Registruj se</a></li>\r\n" + "	</ul>\r\n");
			if (!greska.equals(""))
				retVal.append("	<div>" + greska + "</div>\r\n");
			retVal.append("	<a href=\"index.html\">Povratak</a>\r\n" + "	<br/>\r\n" + "</body>\r\n" + "</html>");

			out.write(retVal.toString());
			return;
		}

		session.setAttribute(KORISNIK_KEY, korisnik);

		response.sendRedirect(bURL + "kategorijaLeka");
	}
	
	/** Retrieve HTML page to display a specific user entity, GET request
	 * @throws IOException */
	// GET: users/details?id=1
	@GetMapping(value="/details")
	@ResponseBody
	public ModelAndView details(@RequestParam String email_korisnika) throws IOException {	
		Korisnik korisnik = korisnikService.findOne(email_korisnika);
		ModelAndView result = new ModelAndView("korisniciDetails");
		result.addObject("korisnik", korisnik);
		return result;
	}
	
	@GetMapping(value="/logout")
	@ResponseBody
	public void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {	

		Korisnik korisnik = (Korisnik) request.getSession().getAttribute(KORISNIK_KEY);
		String greska = "";
		if(korisnik==null)
			greska="korisnik nije prijavljen<br/>";
		
		if(!greska.equals("")) {
			response.setContentType("text/html; charset=UTF-8");
			PrintWriter out;	
			out = response.getWriter();
			
			StringBuilder retVal = new StringBuilder();
			retVal.append(
					"<!DOCTYPE html>\r\n" + 
					"<html>\r\n" + 
					"<head>\r\n" +
					"	<meta charset=\"UTF-8\">\r\n" + 
					"	<base href=\"/PrviMavenVebProjekat/\">	\r\n" + 
					"	<title>Prijava korisnika</title>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviForma.css\"/>\r\n" + 
					"	<link rel=\"stylesheet\" type=\"text/css\" href=\"css/StiloviHorizontalniMeni.css\"/>\r\n" + 
					"</head>\r\n" + 
					"<body>\r\n" + 
					"	<ul>\r\n" + 
					"		<li><a href=\"registracija.html\">Registruj se</a></li>\r\n" + 
					"	</ul>\r\n");
			if(!greska.equals(""))
				retVal.append(
					"	<div>"+greska+"</div>\r\n");
			retVal.append(
					"	<form method=\"post\" action=\"korisnici/login\">\r\n" + 
					"		<table>\r\n" + 
					"			<caption>Prijava korisnika na sistem</caption>\r\n" + 
					"			<tr><th>Email:</th><td><input type=\"text\" value=\"\" name=\"korisnickoIme\" required/></td></tr>\r\n" + 
					"			<tr><th>Šifra:</th><td><input type=\"password\" value=\"\" name=\"lozinka\" required/></td></tr>\r\n" + 
					"			<tr><th></th><td><input type=\"submit\" value=\"Prijavi se\" /></td>\r\n" + 
					"		</table>\r\n" + 
					"	</form>\r\n" + 
					"	<br/>\r\n" + 
					"	<ul>\r\n" + 
					"		<li><a href=\"korisnici/logout\">Odjavi se</a></li>\r\n" + 
					"	</ul>" +
					"</body>\r\n" + 
					"</html>");
			
			out.write(retVal.toString());
			return;
		}
		
		
		request.getSession().removeAttribute(KORISNIK_KEY);
		request.getSession().invalidate();
		response.sendRedirect(bURL);
	}
	
	@PostMapping(value = "/registracija")
	public void registracija(@RequestParam(required = true) String korisnickoIme, @RequestParam(required = true) String lozinka, @RequestParam(required = true) String email, @RequestParam(required = true) String ime,
			@RequestParam(required = true) String prezime, @RequestParam(required = true) String datumRodjenja, @RequestParam(required = true) String adresa, @RequestParam(required = true) String brojTelefona, 
			HttpSession session, HttpServletResponse response) throws IOException {
		UlogaEnum ulogaKorisnika = UlogaEnum.KUPAC;
		LocalDateTime vremeRegistracije = LocalDateTime.now();
		Date datumRodjenjaDate = null;
		try {
			datumRodjenjaDate = new SimpleDateFormat("yyyy-MM-dd").parse(datumRodjenja);
		} catch (ParseException e) {
			System.out.println("greska prilikom parsiranja datuma");
			e.printStackTrace();
		}
		Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumRodjenjaDate, adresa, brojTelefona, vremeRegistracije, ulogaKorisnika);
		korisnikService.save(korisnik);
		
		response.sendRedirect(bURL + "korisnici");
	}
	
	@GetMapping
	public ModelAndView getKorisnici(HttpSession session, HttpServletResponse response){
		List<Korisnik> korisnici = korisnikService.findAll();
		
		// podaci sa nazivom template-a
		ModelAndView rezultat = new ModelAndView("korisnici"); // naziv template-a
		rezultat.addObject("korisnici", korisnici); // podatak koji se šalje template-u

		return rezultat; // prosleđivanje zahteva zajedno sa podacima template-u
	}
	
	@GetMapping(value = "/listaKorisnika")
	@ResponseBody
	public Map<String, Object> getListaKorisnika(HttpSession session, HttpServletResponse response){
		List<Korisnik> korisnici = korisnikService.findAll();
		
		Map<String, Object> odgovor = new LinkedHashMap<>();
		odgovor.put("status", "ok");
		odgovor.put("korisnici", korisnici);
		return odgovor;
	}
	
	@PostMapping(value="/delete")
	public void obrisiKorisnika(@RequestParam String email_korisnika, HttpServletResponse response) throws IOException {				
		korisnikService.delete(email_korisnika);

		response.sendRedirect(bURL+"korisnici");
	}

}
