package com.ftn.PrviMavenVebProjekat.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;

import com.ftn.PrviMavenVebProjekat.service.KategorijaLekaService;


@Controller
@RequestMapping(value = "/kategorijaLeka")
public class KategorijaLekaController implements ServletContextAware {
	
	public static final String KATEGORIJA_LEKA_KEY = "kategorijaLeka";
	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
		
	@Autowired
	private KategorijaLekaService kategorijaLekaService;
	
	
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
//		memorijaAplikacije = applicationContext.getBean(ApplicationMemory.class);
//	    KategorijeLekova kategorijeLekova = new KategorijeLekova(); 
//	    
//		
////		servletContext.setAttribute(KnjigeController.KNJIGE_KEY, knjige);	
//		
//		memorijaAplikacije.put(KATEGORIJA_LEKA_KEY, kategorijeLekova);
	}
	
	/** pribavnjanje HTML stanice za prikaz svih entiteta, get zahtev 
	 * @throws IOException */
	// GET: knjige
	@GetMapping
	public ModelAndView index() {	
		List<KategorijaLeka> kategorijeLekova = kategorijaLekaService.findAll();
		ModelAndView rezultat = new ModelAndView("kategorijaLeka");
		rezultat.addObject("kategorijeLekova", kategorijeLekova);
		return rezultat;
	}
	
	/** pribavnjanje HTML stanice za unos novog entiteta, get zahtev 
	 * @throws IOException */
	// GET: knjige/dodaj
	@GetMapping(value="/add")
	public String create(HttpSession session, HttpServletResponse response){
		return "dodajKategorijuLeka";
	}
	
	/** obrada podataka forme za unos novog entiteta, post zahtev */
	// POST: knjige/add
	@PostMapping(value="/add")
	public void create(@RequestParam String naziv, @RequestParam String namena, 
			@RequestParam String opis, HttpServletResponse response) throws IOException {		

		KategorijaLeka kategorNova = new KategorijaLeka(naziv, namena, opis);
		kategorijaLekaService.save(kategorNova);
		response.sendRedirect(bURL + "/kategorijaLeka");
	}
	
	/** obrada podataka forme za izmenu postojećeg entiteta, post zahtev */
	// POST: knjige/edit
	@PostMapping(value="/edit")
	public void edit(@ModelAttribute KategorijaLeka lekEdited , HttpServletResponse response) throws IOException {	

		kategorijaLekaService.update(lekEdited);
		
		response.sendRedirect(bURL+"kategorijaLeka");
	}
	
	/** obrada podataka forme za za brisanje postojećeg entiteta, post zahtev */
	// POST: knjige/delete
	@PostMapping(value="/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {
		
		KategorijaLeka deleted = kategorijaLekaService.delete(id);
		response.sendRedirect(bURL+"kategorijaLeka");
	}
	
	/** pribavnjanje HTML stanice za prikaz određenog entiteta , get zahtev 
	 * @throws IOException */
	// GET: knjige/details?id=1
	@GetMapping(value="/details")
	@ResponseBody
	public ModelAndView details(@RequestParam Long id) throws IOException {	
		KategorijaLeka kLeka = kategorijaLekaService.findOne(id);
		ModelAndView rezultat = new ModelAndView("kategorijaLekaDetails");
		rezultat.addObject("kategorijaLeka", kLeka);
		return rezultat;
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	

	
}
