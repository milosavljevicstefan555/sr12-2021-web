package com.ftn.PrviMavenVebProjekat.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Korpa;
import com.ftn.PrviMavenVebProjekat.model.KorpaItem;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;
import com.ftn.PrviMavenVebProjekat.model.Racun;
import com.ftn.PrviMavenVebProjekat.model.RacunItem;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.KorpaService;
import com.ftn.PrviMavenVebProjekat.service.LoyaltyKarticaService;
import com.ftn.PrviMavenVebProjekat.service.RacunService;

@RestController
public class RacunController implements ServletContextAware {
	
	@Autowired
	private ServletContext servletContext;
	private  String bURL; 

	@Autowired
	private KorisnikService korisnikService;
    @Autowired
    private KorpaService korpaService;

    @Autowired
    private RacunService racunService;
    
    @Autowired
    private LoyaltyKarticaService loyaltyKarticaService;

    @PostMapping("/racun/izdaj/{korpaId}")
    public void izdajRacun(@PathVariable Long korpaId, @RequestParam(required = false) Integer discount, HttpSession session, HttpServletResponse response) throws IOException {
        Korpa korpa = korpaService.findOne(korpaId);
        if (korpa == null) {
            System.out.println("greska");
        }
        ModelAndView result = new ModelAndView("profil");
        Racun racun = new Racun();
       
        racun.setLekovi(convertToRacunItems(korpa.getItems()));
        racun.setTimeStamp(new java.sql.Date(System.currentTimeMillis())); 

        double cena = calculateCena(korpa.getItems());
        int brojPoena = (int) (cena / 500); // Calculate the number of points based on the price

        // Update the loyalty card points and reduce the used discount
        

        String korisnikKobaja = session.getAttribute("korisnik").toString();
        String[] split = korisnikKobaja.split(";");
        String emailUlogovanogKorisnika = split[2];
        Korisnik korisnik = korisnikService.findOne(emailUlogovanogKorisnika);
        racun.setKupac(korisnik);
        LoyaltyKartica loyaltyKartica = loyaltyKarticaService.findOne(korisnik);
        if (loyaltyKartica != null) {
            int currentPoints = loyaltyKartica.getBrojPoena();
            loyaltyKartica.setBrojPoena(currentPoints - discount + brojPoena); // Reduce the used discount from the loyalty card points
            loyaltyKarticaService.update(loyaltyKartica);

            double popust = discount * 0.05;
            double iznosPopusta = cena * popust;
            double novaCena = cena - iznosPopusta;
            racun.setCena(novaCena);
        } else {
            racun.setCena(cena);
        }
        result.addObject("korisnik", korisnik);
        Racun savedRacun = racunService.save(racun);
        if (savedRacun != null) {
            korpaService.delete(korpaId);
        }
        response.sendRedirect("/PrviMavenVebProjekat/profil");
    }
    @GetMapping("/details/{id}")
    public ModelAndView racunDetails(@PathVariable("id") Long id) {
        Racun racun = racunService.findOne(id);
        ModelAndView rezultat = new ModelAndView("racunDetails");
        rezultat.addObject("racun", racun);
        return rezultat;
    }

    
    @GetMapping("/profil")
    public ModelAndView profil(HttpSession session, HttpServletResponse response) {
    	String korisnikKobaja = session.getAttribute("korisnik").toString();
		String[] split = korisnikKobaja.split(";");
		String emailUlogovanogKorisnika = split[2];
		System.out.println(korisnikKobaja);
		System.out.println(emailUlogovanogKorisnika);
        Korisnik korisnik = korisnikService.findOne(emailUlogovanogKorisnika);
        List<Racun> istorijaKupovine = racunService.findPurchaseHistoryForUser(korisnik);
        
        ModelAndView result = new ModelAndView("profil");
        // Add the purchase history list to the model
        result.addObject("korisnik", korisnik);
        result.addObject("istorijaKupovine", istorijaKupovine);
        
        // Return the view name
        return result;
    }

    private double calculateCena(List<KorpaItem> lekovi) {
        double total = 0.0;
        for (KorpaItem item : lekovi) {
            total += item.getLek().getCena() * item.getKolicina();
        }
        return total;
    }
    private List<RacunItem> convertToRacunItems(List<KorpaItem> korpaItems) {
        List<RacunItem> racunItems = new ArrayList<>();
        for (KorpaItem korpaItem : korpaItems) {
            RacunItem racunItem = new RacunItem();
            racunItem.setLek(korpaItem.getLek());
            racunItem.setKolicina(korpaItem.getKolicina());
            racunItems.add(racunItem);
        }
        return racunItems;
    }

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}
}
