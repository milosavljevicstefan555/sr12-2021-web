package com.ftn.PrviMavenVebProjekat.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Korpa;
import com.ftn.PrviMavenVebProjekat.model.KorpaItem;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.model.LoyaltyKartica;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.KorpaItemService;
import com.ftn.PrviMavenVebProjekat.service.KorpaService;
import com.ftn.PrviMavenVebProjekat.service.LekService;
import com.ftn.PrviMavenVebProjekat.service.LoyaltyKarticaService;

@Controller
@RequestMapping(value = "/korpa")
public class KorpaController  implements ServletContextAware {
	
	public static final String KORPA_KEY = "korpa";
	
	@Autowired
	private ServletContext servletContext;
	private String bUrl;
	
	@Autowired
	private KorpaService korpaService;
	@Autowired
	private KorpaItemService korpaItemService;
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private LekService lekService;
	@Autowired
	private LoyaltyKarticaService loyaltyKarticaService;
	
	@PostConstruct
	public void init() {
		bUrl = servletContext.getContextPath() + "/";
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	@GetMapping
	public ModelAndView getKorpa(HttpSession session, HttpServletResponse response) {
	    String korisnikKobaja = session.getAttribute("korisnik").toString();
	    String[] split = korisnikKobaja.split(";");
	    String emailUlogovanogKorisnika = split[2];
	    System.out.println(korisnikKobaja);
	    System.out.println(emailUlogovanogKorisnika);
	    Korpa korpa = null;
	    Korisnik korisnik = korisnikService.findOne(emailUlogovanogKorisnika);
	    LoyaltyKartica loyalty = loyaltyKarticaService.findOne(korisnik);
	    for (Korpa k : korpaService.findAll()) {
	        if (k.getKorisnik().getEmail().equals(emailUlogovanogKorisnika)) {
	            korpa = k;
	        }
	    }
	    // korpa ne postoji
	    if (korpa == null) {
	        korpa = new Korpa(korisnik, null);
	    }
	    
	    ModelAndView result = new ModelAndView("korpa");
	    result.addObject("loyaltyKartica", loyalty);
	    result.addObject("korpa", korpa);
	    result.addObject("discount", 0);
	    return result;
	}

	
	@PostMapping("/updateKolicina")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> updateKolicina(@RequestParam("index") int index, @RequestParam("value") int value, HttpSession session) {
		String korisnikKobaja = session.getAttribute("korisnik").toString();
		String[] split = korisnikKobaja.split(";");
		String emailUlogovanogKorisnika = split[2];
		System.out.println(korisnikKobaja);
		System.out.println(emailUlogovanogKorisnika);
		Korpa korpa = null;
		for (Korpa k : korpaService.findAll()) {
			if (k.getKorisnik().getEmail().equals(emailUlogovanogKorisnika)) {
				korpa = k;
			}
		}
        if (korpa != null && index >= 0 && index < korpa.getItems().size()) {
            KorpaItem korpaItem = korpa.getItems().get(index);
            int updatedKolicina = korpaItem.getKolicina() + value;
            Lek lek = korpaItem.getLek();
            lek.setDostupnaKolicina(lek.getDostupnaKolicina() - value);
            if (updatedKolicina >= 0) {
                korpaItem.setKolicina(updatedKolicina);
                lekService.update(lek);
                korpaItemService.update(korpaItem);
                Map<String, Object> response = new HashMap<>();
                response.put("kolicina", korpaItem.getKolicina());
                response.put("message", "Kolicina updated successfully");

                return ResponseEntity.ok(response);
            }
        }
        Map<String, Object> errorResponse = new HashMap<>();
        errorResponse.put("message", "Invalid request");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
	
	@PostMapping("/addToCart")
	@ResponseBody
	public ResponseEntity<String> addToCart(@RequestParam("lekId") Long lekId, HttpSession session) {
	    Lek lek = lekService.findOne(lekId);
	    if (lek != null) {
	    	String korisnikKobaja = session.getAttribute("korisnik").toString();
			String[] split = korisnikKobaja.split(";");
			String emailUlogovanogKorisnika = split[2];
			System.out.println(korisnikKobaja);
			System.out.println(emailUlogovanogKorisnika);
			Korpa korpa = null;
			System.out.println(korpaService.findAll());
			for (Korpa k : korpaService.findAll()) {
				System.out.println(k.getKorisnik().getEmail() + "aaaaaaaaa");
				if (k.getKorisnik().getEmail().equals(emailUlogovanogKorisnika)) {
					korpa = k;
				}
			}
	        if (korpa == null) {
	            korpa = new Korpa();
	            korpa.setKorisnik(korisnikService.findOne(emailUlogovanogKorisnika));
	            korpaService.save(korpa);
	            for (Korpa k : korpaService.findAll()) {
					System.out.println(k.getKorisnik().getEmail() + "bbbbbbbb");
					if (k.getKorisnik().getEmail().equals(emailUlogovanogKorisnika)) {
						korpa = k;
					}
				}
	        }
	        List<KorpaItem> items = korpa.getItems();
	        // Check if the drug is already in the cart
	        for (KorpaItem item : items) {
	            if (item.getLek().getId() == lekId) {
	                item.setKolicina(item.getKolicina() + 1);
	                lek.setDostupnaKolicina(lek.getDostupnaKolicina() - 1);
	                lekService.update(lek);
	                korpaItemService.update(item);
	                return ResponseEntity.ok("Lek je dodat u korpu.");
	            }
	        }
	        // If the drug is not in the cart, create a new item
	        KorpaItem newItem = new KorpaItem();
	        newItem.setLek(lek);
	        newItem.setKolicina(1);
	        lek.setDostupnaKolicina(lek.getDostupnaKolicina() - 1);
	        items.add(newItem);
	        lekService.update(lek);
	        korpaItemService.save(newItem, korpa.getId());
	        return ResponseEntity.ok("Lek je dodat u korpu.");
	    }
	    return ResponseEntity.badRequest().body("Invalid request.");
	}

	

}
