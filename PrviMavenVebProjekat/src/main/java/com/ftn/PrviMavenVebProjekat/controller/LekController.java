package com.ftn.PrviMavenVebProjekat.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.PrviMavenVebProjekat.bean.OblikEnum;
import com.ftn.PrviMavenVebProjekat.bean.UlogaEnum;
import com.ftn.PrviMavenVebProjekat.model.KategorijaLeka;
import com.ftn.PrviMavenVebProjekat.model.Komentar;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.model.ProizvodjacLeka;
import com.ftn.PrviMavenVebProjekat.service.KategorijaLekaService;
import com.ftn.PrviMavenVebProjekat.service.KomentarService;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.LekService;
import com.ftn.PrviMavenVebProjekat.service.ProizvodjacLekaService;

@Controller
@RequestMapping(value = "/lek")
public class LekController implements ServletContextAware {
	
	public static final String LEK_KEY = "lek";
	
	@Autowired
	private ServletContext servletContext;
	private String bURL;
	
	@Autowired
	private LekService lekService;
	
	@Autowired
	private KomentarService komentarService;
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private KategorijaLekaService kategorijaLekaService;
	@Autowired
	private ProizvodjacLekaService proizvodjacLekaService;
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
		
	}
	@PostConstruct
	public void init() {	
		bURL = servletContext.getContextPath()+"/";
//		memorijaAplikacije = applicationContext.getBean(ApplicationMemory.class);
//	    KategorijeLekova kategorijeLekova = new KategorijeLekova(); 
//	    
//		
////		servletContext.setAttribute(KnjigeController.KNJIGE_KEY, knjige);	
//		
//		memorijaAplikacije.put(KATEGORIJA_LEKA_KEY, kategorijeLekova);
	}

	@GetMapping
	public ModelAndView index(HttpSession session, HttpServletResponse response) {	
		String korisnikKobaja = session.getAttribute("korisnik").toString();
		String[] split = korisnikKobaja.split(";");
		String emailUlogovanogKorisnika = split[2];
		System.out.println(korisnikKobaja);
		System.out.println(emailUlogovanogKorisnika);
		Korisnik korisnik = korisnikService.findOne(emailUlogovanogKorisnika);
		
		List<Lek> lekovi = lekService.findAll();
		if (korisnik.getUlogaKorisnika() == UlogaEnum.KUPAC) {
		    Iterator<Lek> iterator = lekovi.iterator();
		    while (iterator.hasNext()) {
		        Lek lek = iterator.next();
		        if (lek.getDostupnaKolicina() == 0) {
		            iterator.remove();
		        }
		    }
		}
		ModelAndView rezultat = new ModelAndView(LEK_KEY);
		rezultat.addObject("lekovi", lekovi);
		return rezultat;
	}
	@GetMapping("/add")
	public ModelAndView showDodajLekForm(HttpSession session, HttpServletResponse response) {
		ModelAndView retVal = new ModelAndView("dodajLek");
	    // Get the list of kategorije and proizvodjaci from the database or any other source
	    List<KategorijaLeka> kategorije = kategorijaLekaService.findAll();
	    List<ProizvodjacLeka> proizvodjaci = proizvodjacLekaService.findAll();
	    
	    // Add the kategorije and proizvodjaci to the model
	    retVal.addObject("kategorije", kategorije);
	    retVal.addObject("proizvodjaci", proizvodjaci);
	    
	    // Render the dodajLek.html template
	    return retVal;
	}
	@PostMapping(value = "/add")
	public void addLek(@RequestParam("naziv") String naziv,
	                   @RequestParam("opis") String opis,
	                   @RequestParam("kontraindikacije") String kontraindikacije,
	                   @RequestParam("oblik") OblikEnum oblik,
	                   @RequestParam("dostupnaKolicina") int dostupnaKolicina,
	                   @RequestParam("cena") double cena,
	                   @RequestParam("slikaURL") String slikaURL,
	                   @RequestParam("proizvodjacId") Long proizvodjacId,
	                   @RequestParam("kategorijaId") Long kategorijaId,
	                   HttpSession session, HttpServletResponse response) {
	    // Create the Lek object using the received data
	    Lek lek = new Lek();
	    lek.setNaziv(naziv);
	    lek.setOpis(opis);
	    lek.setKontraindikacije(kontraindikacije);
	    lek.setOblik(oblik);
	    lek.setDostupnaKolicina(dostupnaKolicina);
	    lek.setCena(cena);
	    lek.setSlikaURL(slikaURL);
	    
	    ProizvodjacLeka proizvodjac = proizvodjacLekaService.findOne(proizvodjacId);
	    KategorijaLeka kategorija = kategorijaLekaService.findOne(kategorijaId);
	    
	    lek.setProizvodjac(proizvodjac);
	    lek.setKategorija(kategorija);
	    lekService.save(lek);

	    try {
			response.sendRedirect(bURL + "/lek");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Replace "bURL" with your actual base URL
	}

	@GetMapping("/search")
	public ModelAndView search(HttpSession session, HttpServletResponse response,@RequestParam(required = true) String search) {	
		String korisnikKobaja = session.getAttribute("korisnik").toString();
		String[] split = korisnikKobaja.split(";");
		String emailUlogovanogKorisnika = split[2];
		System.out.println(korisnikKobaja);
		System.out.println(emailUlogovanogKorisnika);
		Korisnik korisnik = korisnikService.findOne(emailUlogovanogKorisnika);
	    List<Lek> lekovi;
	    if (search != null && !search.isEmpty()) {
	        // Perform search based on the search term
	        lekovi = lekService.searchLekovi(search);
	    } else {
	        // Fetch all lekovi if no search term is provided
	        lekovi = lekService.findAll();
	    }
	    if (korisnik.getUlogaKorisnika() == UlogaEnum.KUPAC) {
			for (Lek l: lekovi) {
				if (l.getDostupnaKolicina() == 0) {
					lekovi.remove(l);
				}
			}
		}
	    ModelAndView rezultat = new ModelAndView(LEK_KEY);
	    rezultat.addObject("lekovi", lekovi);
	    return rezultat;
	}

	
	@GetMapping(value="/details")
	@ResponseBody
	public ModelAndView details(@RequestParam Long id) throws IOException {	
		Lek lek = lekService.findOne(id);
		ArrayList<Komentar> komentari = komentarService.findAll();
		for (Komentar k : komentari) {
			if (k.getLek().getId() != lek.getId()) {
				komentari.remove(k);
			}
		}
		ModelAndView rezultat = new ModelAndView("lekDetails");
		rezultat.addObject("komentari", komentari);
		rezultat.addObject(LEK_KEY, lek);
		return rezultat;
	}
}
