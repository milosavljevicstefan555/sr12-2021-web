package com.ftn.PrviMavenVebProjekat.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.PrviMavenVebProjekat.model.Komentar;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.Lek;
import com.ftn.PrviMavenVebProjekat.service.KomentarService;
import com.ftn.PrviMavenVebProjekat.service.KorisnikService;
import com.ftn.PrviMavenVebProjekat.service.LekService;

@Controller
@RequestMapping(value = "/komentar")
public class KomentarController implements ServletContextAware{
	
	@Autowired
	private ServletContext servletContext;
	
	private String bURL;
	
	@Autowired
	private KomentarService komentarService;
	
	@Autowired
	private LekService lekService;
	
	@Autowired
	private KorisnikService korisnikService;

	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	@GetMapping
	public ModelAndView index() {	
		List<Komentar> komentari = komentarService.findAll();
		ModelAndView rezultat = new ModelAndView("komentar");
		rezultat.addObject("komentari", komentari);
		return rezultat;
	}

	/** pribavnjanje HTML stanice za unos novog entiteta, get zahtev 
	 * @throws IOException */
	// GET: komentari/dodaj
	@GetMapping(value="/add")
	public ModelAndView create(HttpSession session, HttpServletResponse response){
		ArrayList<Lek> sviLekovi = lekService.findAll();
	    ModelAndView result = new ModelAndView("dodajKomentar");
	    result.addObject("lekovi", sviLekovi);
		return result;
	}

	/** obrada podataka forme za unos novog entiteta, post zahtev */
	// POST: komentari/add 
	@PostMapping(value="/add")
	public void create(@RequestParam String tekstKomentara, @RequestParam int ocena, @RequestParam String email_autor,
	@RequestParam Long lek_id, @RequestParam boolean anoniman, HttpServletResponse response) throws IOException {
		Korisnik autor = korisnikService.findOne(email_autor);
		Lek lek = lekService.findOne(lek_id);
		LocalDate localDate = LocalDate.now();
		Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Komentar komentarNovo = new Komentar(tekstKomentara, ocena, date, autor, lek, anoniman);
		komentarService.save(komentarNovo);
		response.sendRedirect(bURL + "/komentar");
	}
	
	@PostMapping(value = "/submitComment")
	public void submitComment(@RequestParam("comment") String comment,
	                          @RequestParam("rating") int rating,
	                          @RequestParam("lekId") Long lekId,
	                          HttpSession session, HttpServletResponse response) throws IOException {
		String korisnikKobaja = session.getAttribute("korisnik").toString();
		String[] split = korisnikKobaja.split(";");
		String emailUlogovanogKorisnika = split[2];
		System.out.println(korisnikKobaja);
		System.out.println(emailUlogovanogKorisnika);
		
		boolean anoniman = false;
	    
	    Korisnik autor = korisnikService.findOne(emailUlogovanogKorisnika);
	    Lek lek = lekService.findOne(lekId);
	    LocalDate localDate = LocalDate.now();
	    Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
	    
	    Komentar komentarNovo = new Komentar(comment, rating, date, autor, lek, anoniman);
	    komentarService.save(komentarNovo);
	    
	    double prosecnaOcena = komentarService.calculateAverageRatingForLek(lekId);
	    lek.setProsecnaOcena(prosecnaOcena);
	    lekService.update(lek);
	    
	    response.sendRedirect(bURL + "/lek"); 
	}




	/** obrada podataka forme za izmenu postojećeg entiteta, post zahtev */
	// POST: komentari/edit
	@PostMapping(value="/edit")
	public void edit(@RequestParam Long id, @RequestParam String tekstKomentara, @RequestParam int ocena,
			@RequestParam String datumObjavljivanjaString, @RequestParam String email_autor,
			@RequestParam Long lek_id, @RequestParam boolean anoniman, HttpServletResponse response) throws IOException {	
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(datumObjavljivanjaString);
		} catch (ParseException e) {
			System.out.println("nije uspelo pretvaranje datuma iz baze");
			e.printStackTrace();
		}
		Korisnik autor = korisnikService.findOne(email_autor);
		Lek lek = lekService.findOne(lek_id);
		Komentar komentarEdited = new Komentar(id, tekstKomentara, ocena, date, autor, lek, anoniman);
		komentarService.update(komentarEdited);
		
		response.sendRedirect(bURL+"komentar");
	}
	
	/** Retrieve HTML page for displaying a specific Komentar entity, GET request

    @throws IOException */
    // GET: comments/details?id=1
    @GetMapping(value="/details")
    @ResponseBody
    public ModelAndView details(@RequestParam Long id) throws IOException {
    Komentar komentar = komentarService.findOne(id);
    ArrayList<Lek> sviLekovi = lekService.findAll();
    ModelAndView result = new ModelAndView("komentarDetails");
    result.addObject("komentar", komentar);
    result.addObject("lekovi", sviLekovi);
    return result;
    }

	/** obrada podataka forme za za brisanje postojećeg entiteta, post zahtev */
	// POST: komentari/delete
	@PostMapping(value="/delete")
	public void delete(@RequestParam Long id, HttpServletResponse response) throws IOException {
		
		Komentar deleted = komentarService.delete(id);
		response.sendRedirect(bURL+"komentar");
	}
}
